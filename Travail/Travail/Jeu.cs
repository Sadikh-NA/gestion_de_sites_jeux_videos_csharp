﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Travail
{
    // type Enumération avec ses 3 valeurs possibles
    public enum Categorie { Course,Action, Autres }
    
    /*
     * Classe Jeu qui implémente l'interface IEquatable pour de la methode Equals
     * et l'interface Icomporable pour la methode compareTo
     */
    public class Jeu : IEquatable<Jeu>, IComparable<Jeu>
    {

        //Déclaration des attributs
        private string nom;
        private string description;
        private string editeur;
        private float prix;
        private string plateforme;
        private Boolean disponibilite;
        private DateTime dateSortie;
        private Categorie genre;
        private Image photo;

        //Constructeurs par défaut
        public Jeu()
        {
            this.nom = "";
            this.description = "";
            this.editeur = "";
            this.plateforme = "";
            this.prix = 0;
            this.disponibilite = false;
            this.dateSortie = new DateTime();
            this.genre = Categorie.Autres; 
        }

        // Accesseurs et mutateurs des attributs
        public string Nom { get => nom; set => nom = value; }
        public string Description { get => description; set => description = value; }
        public string Editeur { get => editeur; set => editeur = value; }
        public string Plateforme { get => plateforme; set => plateforme = value; }
        public bool Disponibilite { get => disponibilite; set => disponibilite = value; }
        public DateTime DateSortie { get => dateSortie; set => dateSortie = value; }
        public float Prix { get => prix; set => prix = value; }
        internal Categorie Genre { get => genre; set => genre = value; }
        public Image Photo { get => photo; set => photo = value; }


        //Constructeur avec paramètres
        public Jeu(string nom, string description, string editeur, string plateforme, Categorie genre, float prix, DateTime dateS, Boolean recond, Image photo)
        {
            this.nom = nom;
            this.description = description;
            this.editeur = editeur;
            this.plateforme = plateforme;
            this.genre = genre;
            this.prix = prix;
            this.dateSortie = dateS;
            this.disponibilite  = recond;
            this.Photo = photo;

        }

        //Conversion de l'énumeration en string
        public string GenreS
        {
            get { return Enum.Format(typeof(Categorie), this.genre, "g"); }
            set { this.genre = (Categorie)Enum.Parse(typeof(Categorie), value, false); }
        }

        
        //surcharge de la methode ToString 
        public override string ToString()
        {
            string s = "";
            s+= "\n Nom: " + this.nom;
            s+= "\n Editeur: " + this.editeur;
            s+= "\n Catégorie: " + this.GenreS;
            s+= "\n Disponibilité: " + this.disponibilite;
            s+= "\n Prix: " + this.prix;
            s+= "\n Plateforme: " + this.plateforme;
            s+= "\n Description: " + this.description;
            s+= "\n Image: " + this.Photo;
            s += "\n Date de Sortie: " + this.dateSortie;
            return s;
        }

        //Fonction de saiie pour l'ajout d'un jeu
        public void Saisie()
        {
            string s;
            this.disponibilite = false;
            Console.WriteLine("Nom");
            this.nom = Console.ReadLine();
            Console.WriteLine("Editeur");
            this.editeur = Console.ReadLine();
            string genre;
            do
            {
                Console.WriteLine("Catégorie: Course/Action/Autres");
                genre = Console.ReadLine();

            } while (!genre.Equals("Course") && !genre.Equals("Action") && !genre.Equals("Autres"));
            
            this.GenreS = genre;
            Console.WriteLine("Disponibilité: Vrai/Faux");
            s=Console.ReadLine();
            if (s.ToLower() == "Vrai".ToLower()) { this.disponibilite = true; }
            Boolean test = false;
            do
            {
                Console.WriteLine("Prix");
                try
                {
                    this.prix=Single.Parse(Console.ReadLine());

                }
                catch
                {
                    Console.WriteLine("Vous n'avez pas saisi un nombre ");
                    test = true;
                }
            } while (test);
            Console.WriteLine("Prix");
            Console.WriteLine("Plateforme");
            this.plateforme = Console.ReadLine();
            Console.WriteLine("Description");
            this.description = Console.ReadLine();
            Console.WriteLine("Image");
            this.Photo = null;
            this.dateSortie = DateTime.Now; 
        }

        //methode pour afficher le afficher ToString
        public void Afficher()
        {
            Console.WriteLine(this.ToString());
        }

        //Surcharge de la methode Equals de IEquatable
        public override bool Equals(object obj) 
        {
            if (obj == null)
                return false;
            Jeu i = obj as Jeu;
            if (i == null)
                return false;
            else
                return Equals(obj);
        }


        //Implémentation de la methode Equals
        public bool Equals(Jeu i)
        {
            if (!(i is Jeu) || i == null)
                return false;
            else
            {
                return this.nom.Equals(i.nom);
            }

        }


        //surcharge de l'opérateur == sur deux objects
        public static bool operator ==(Jeu i1, Jeu i2)
        {
            if ((object)i1 == null)
                return (object)i2 == null;
            return i1.Equals(i2);
        }

        //surcharge de l'opérateur != sur deux objects
        public static bool operator !=(Jeu i1, Jeu i2)
        {
            return !(i1 == i2);
        }
        /*
         *Compare l'instance actuelle avec un autre objet du même type et retourne un entier 
         *qui indique si l'instance actuelle précède ou suit un autre objet ou se trouve à la même position dans l'ordre de tri
         */
        public int CompareTo(Jeu i)
        {
            if (i == null || !(i is Jeu)) return 0;
            return this.prix.CompareTo(i.prix);
        }

        //surcharge de l'opérateur > sur deux objects
        public static bool operator >(Jeu operand1, Jeu operand2)
        {
            return operand1.CompareTo(operand2) == 1;
        }

        // Define the is less than operator.
        public static bool operator <(Jeu operand1, Jeu operand2)
        {
            return operand1.CompareTo(operand2) == -1;
        }

        //surcharge de l'opérateur <= sur deux objects
        public static bool operator <=(Jeu left, Jeu right)
        {
            return left.CompareTo(right) <= 0;
        }

        //surcharge de l'opérateur <= sur deux objects
        public static bool operator >=(Jeu left, Jeu right)
        {
            return left.CompareTo(right) >= 0;
        }

        //surcharge de la methode HashCode
        public override int GetHashCode()
        {
            int hashCode = -1941401972;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(nom);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(description);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(editeur);
            hashCode = hashCode * -1521134295 + prix.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(plateforme);
            hashCode = hashCode * -1521134295 + disponibilite.GetHashCode();
            hashCode = hashCode * -1521134295 + dateSortie.GetHashCode();
            hashCode = hashCode * -1521134295 + genre.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Image>.Default.GetHashCode(Photo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nom);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Description);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Editeur);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Plateforme);
            hashCode = hashCode * -1521134295 + Disponibilite.GetHashCode();
            hashCode = hashCode * -1521134295 + DateSortie.GetHashCode();
            hashCode = hashCode * -1521134295 + Prix.GetHashCode();
            hashCode = hashCode * -1521134295 + Genre.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(GenreS);
            return hashCode;
        }
    }
}
