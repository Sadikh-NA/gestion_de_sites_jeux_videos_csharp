﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travail
{

    //Class Catalogue
    public class Catalogue
    {
        //Attributs de la classe
        private DateTime dateMAJ;
        private List<Jeu> lesJeux;

        //Constructeur par défaut
        public Catalogue()
        {
            this.dateMAJ = new DateTime();
            this.lesJeux = new List<Jeu>();
        }

        //Constructeur par paramètre
        public Catalogue(List<Jeu> lesJeux, DateTime dateM)
        {
            this.dateMAJ = dateM;
            this.lesJeux = lesJeux;
        }

        //Accesseurs mutateurs de l'attribut DateMAJ
        public DateTime DateMAJ
        {
            get { return this.dateMAJ; }
            set { this.dateMAJ = value; }
        }

        //methode pour ccompter le nombvre de jeux
        public int GetNbJeu()
        {
            return this.lesJeux.Count;
        }

        //methode d'ajout d'un jeu
        public void AjoutJeu(Jeu j)
        {
            this.lesJeux.Add(j);

        }

        //méthode de suppression d'un jeu
        public void SupprimeJeu(Jeu j)
        {
            this.lesJeux.Remove(j);
        }

        //surcharge de la methode ToString
        public override string ToString()
        {
            string s = "";
            foreach (Jeu unJeu in lesJeux)
            {
                s += "\n " + unJeu;
            }
            return s;
        }

        //
        public List<Jeu> GetJeuxGenre(Categorie g)
        {
            List<Jeu> lesjeux = new List<Jeu>();
            foreach (Jeu unJeu in lesjeux)
            {
                if (unJeu.Genre.Equals(g))
                {
                    lesjeux.Add(unJeu);
                }
            }
            return lesjeux;
        }
    }
}
