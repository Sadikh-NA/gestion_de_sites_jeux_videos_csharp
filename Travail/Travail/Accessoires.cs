﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travail
{
    public class Accessoires
    {
        private List<Accessoire> liste;
        public List<Accessoire> Liste
        {
            get { return this.liste; }
            set { this.liste = value; }
        }

        public Accessoires()
        {
            this.liste = new List<Accessoire>();
        }
            
        public Accessoires(List<Accessoire> liste)
        {
            this.liste = liste;

        }

        public void AjoutAccessoire(Accessoire j)
        {
            this.liste.Add(j);
        }
        public void SupprimeAccessoire(Accessoire j)
        {
            this.liste.Remove(j);
        }

        public int NbAccessoire()
        {
            return this.liste.Count;
        }

        public Accessoire GetAccessoireIndice(int i)
        {
            return this.liste.ElementAt(i);

        }

        public Accessoire GetAccessoireNom(string nom)
        {
            return (Accessoire)this.Liste.Find(j => j.Nom.Contains(nom));
        }


        public override string ToString()
        {
            string s = "";
            foreach (Accessoire unAccessoire in this.liste)
            {
                s += "\n " + unAccessoire;
            }
            return s;
        }
    }
}