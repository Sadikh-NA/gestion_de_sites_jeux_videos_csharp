﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travail
{
    //Classe générique Panier qui implémente l'interface IEquata    ble
    //Eviter les FOREACH sur Cette classe
    class Panier<T> where T : IEquatable<T>
    {
        private readonly int tailleMax = 10;      //taille max du tableau
        private int nbElement;      //nb d'elements dans le tableau
        private T[] tab;            //tableau d'éléments

        public int TailleMax { get { return tailleMax; } }
        public int NbElement { get { return nbElement; } set { nbElement = value; } }

        public void Ajout(T elt)
        {
            //S'il reste de la place dans le panier
            if (nbElement < tailleMax)
            {
                bool present = false;
                //Vérification si le produit est déjà dans le panier
                for (int i = 0; i < nbElement; i++)
                {
                    if (elt.Equals(tab[i]))
                    {
                        Console.WriteLine("Action impossible : Produit /déjà dans le panier");
                        present = true;
                    }
                }
                if (!present)
                {
                    tab[nbElement] = elt;
                    nbElement++;
                }

            }
            //Si la taille maximale du panier est atteint
            else
            {
                Console.WriteLine("Action impossible : taille maxi du panier atteint");
            }
        }

        public void Insere(T elt, int pos)
        {
            //S'il reste de la place dans le panier
            if (nbElement < tailleMax && pos < nbElement)
            {
                bool present = false;
                //Vérification si le produit est déjà dans le panier
                for (int i = 0; i < nbElement; i++)
                {
                    if (elt.Equals(tab[i]))
                    {
                        Console.WriteLine("Action impossible : Produit déjà dans le panier");
                        present = true;
                    }

                }
                if (!present)
                {
                    //On créée un tableau temporaire
                    T[] temp = new T[nbElement + 1];

                    //On copie tab dans temp jusqu'à l'élément à ajouter
                    for (int i = 0; i < pos; i++)
                    {
                        temp[i] = tab[i];
                    }
                    temp[pos] = elt;
                    //On copie tab dans temp après l'élément à ajouter.
                    for (int i = pos; i < nbElement; i++)
                    {
                        temp[i + 1] = tab[i];
                    }
                    //temp est le tableau qu'on souhaite. On le copie dans tab
                    tab = temp;
                    nbElement++;
                }


            }
            //Si la taille maximale du panier est atteint ou que la position est trop grande
            else
            {
                Console.WriteLine("Action impossible: position incorrecte ou panier déjà remplit");
            }
        }

        public void Supprime(int indice)
        {
            if (indice < nbElement)
            {
                //On créée un tableau temporaire
                T[] temp = new T[nbElement - 1];

                //On copie tab dans temp jusqu'à l'élément à supprimer
                for (int i = 0; i < indice; i++)
                {
                    temp[i] = tab[i];
                }
                //On copie tab dans temp après l'élément à supprimer.
                for (int i = indice; i < nbElement - 1; i++)
                {
                    temp[i] = tab[i + 1];
                }
                //temp est le tableau qu'on souhaite. On le copie dans tab
                tab = temp;
                nbElement--;
            }
            else
            {
                Console.WriteLine("Action impossible : indice trop grand");
            }


        }


        //Méthode de RECHERCHE
        public T GetIndice(int indice)
        {
            if (indice < nbElement)
            {
                return tab[indice];
            }
            Console.WriteLine("Recherche impossible: indice trop grand");
            return default(T);

        }

        public void Tri()
        {
            if (nbElement > 0)
            {
                for (int i = 0; i < nbElement; i++)
                {
                    for (int j = i; j < nbElement; j++)
                    {
                        if (!Inferieur(tab[i], tab[j]))
                        {
                            T temp = tab[i];
                            tab[i] = tab[j];
                            tab[j] = temp;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Action Impossible : Aucun produit dans le panier .");
            }
        }

        public void Affiche()
        {
            Console.WriteLine(this.ToString());

        }

        public override string ToString()
        {
            string s = "\n";
            for (int i = 0; i < nbElement; i++)
            {
                s += tab[i].ToString() + "\n";
            }
            return s;
        }

        public override bool Equals(object obj)
        {
            T i = (T)obj;
            return this.Equals(obj);
        }

        public bool Equals(T i)
        {
            return this.Equals(i);
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
            //return this.reference;
        }

        public bool Inferieur(T i1, T i2)
        {
            var result = Comparer<T>.Default.Compare(i1, i2);
            if (result < 0) { return true; }
            else { return false; }
        }


        public Panier()
        {
            this.nbElement = 0;
            this.tab = new T[tailleMax];
        }


    }
}
