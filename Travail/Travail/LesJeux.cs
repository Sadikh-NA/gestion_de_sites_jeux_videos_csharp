﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travail
{
    //Classe les Jeux
    public class LesJeux
    {
        //attributs
        private List<Jeu> liste;
        private List<JeuRetro> listeR;

        //Accesseurs mutateurs
        public List<Jeu> Liste{ get { return this.liste; } set { this.liste = value; } }
        public List<JeuRetro> ListeR { get { return this.listeR; } set { this.listeR = value; }}

        //Constructeurs par défauts
        public LesJeux()
        {
            this.liste = new List<Jeu>();
            this.listeR = new List<JeuRetro>();
        }

        //Constructeurs avec parametres
        public LesJeux(List<Jeu> liste)
        {
            this.liste = liste;
        }
        public LesJeux(List<JeuRetro> listeR)
        {
            this.listeR = listeR;
        }

        //Methode pour ajouter un jeu dans la liste des jeux
        public void AjoutJeu(Jeu j)
        {
            this.liste.Add(j);
        }

        //Methode pour supprimer un jeu dans la liste des jeux
        public void SupprimeJeu(Jeu j)
        {
            this.liste.Remove(j);
        }

        public void Supprime(string j)
        {
            Jeu x=GetJeuNom(j);
            if (x != null) { this.liste.Remove(x); }
            else
            {
               Console.WriteLine("Ce jeu n'existe pas");
            }
        }

        //Methode pour compter le nombre de jeu dans la liste des jeux
        public int NbJeu()
        {
            return this.liste.Count;
        }

        //Methode pour ajouter un jeuRetro dans la liste des jeux
        public void AjoutJeuRetro(JeuRetro j)
        {
            this.listeR.Add(j);
            this.liste.Add(j);
        }

        //Methode pour supprimer un jeuRetro dans la liste des jeux
        public void SupprimeJeuRetro(JeuRetro j)
        {
            this.listeR.Remove(j);
        }

        //Methode pour Compter le nombre jeuRetro dans la liste des jeux
        public int NbJeuRetro()
        {
            return this.listeR.Count;
        }

        //Methode pour recupérer le jeu qui est à l'indice(la position) donner en paramètre
        public Jeu GetJeuIndice(int i)
        {
            return this.liste.ElementAt(i);
        }

        //Methode pour recupérer le jeu qui est à le nom donner en paramètre
        public Jeu GetJeuNom(string nom)
        {
            return (Jeu)this.Liste.Find(j =>(j.Nom.ToLower()).Contains((nom.ToLower())));

        }
        //Methode pour recupérer le jeu retro qui est à l'indice(la position) donner en paramètre
        public Jeu GetJeuRetroIndice(int i)
        {
            return this.listeR.ElementAt(i);
        }

        //Methode pour recupérer le jeu rétro qui est à le nom donner en paramètre
        public Jeu GetJeuRetroNom(string nom)
        {
            return (Jeu)this.ListeR.Find(j => (j.Nom.ToLower()).Contains((nom.ToLower())));

        }

        //Cette fonction liste les jeux en fonction du catégorie cité en parametre
        public List<Jeu> GetJeuxGenre(Categorie g)
        {
            List<Jeu> lesJeux = new List<Jeu>();
            foreach(Jeu unjeu in lesJeux)
            {
                if (unjeu.Genre.Equals(g))
                {
                    lesJeux.Add(unjeu);
                }
            }
            return lesJeux;
        }

        //surcharge de la méthode toString
        public override string ToString()
        {
            string s = "";
            foreach (Jeu unjeu in this.liste)
            {
                s += "\n "+unjeu.ToString();
            }
            return s;
        }

        public string ToStrings()
        {
            string s = "";
            foreach (JeuRetro unjeu in this.listeR)
            {
                s += "\n " + unjeu.ToString();
            }
            return s;
        }

        //La méthode pour Trier les Jeux par prix
        public void Trier()
        {
            //une fonction trie bulle

            //taille de tableau
            int MaxTableau = liste.Count();
            //Parcourir le tableau de l'avant dernier objet au 1er objet JEU
            for (int I = MaxTableau - 2; I >= 0; I--)
            {
                //on part du 1 objet à l'objet actuel 
                for (int J = 0; J <= I; J++)
                {
                    /*
                     * on compare si le prix de l'objet qui est apres lui est inférieur si oui on permute 
                     * sinon on fait rien
                     */
                    //p1.nom.CompareTo(p2.nom)
                    if (liste.ElementAt(J + 1).Nom.CompareTo(liste.ElementAt(J).Nom) < 0)
                    {
                        Jeu temp = liste.ElementAt(J + 1);
                        liste.RemoveAt(J + 1);
                        liste.Insert(J + 1, liste.ElementAt(J));
                        liste.RemoveAt(J);
                        liste.Insert(J, temp);

                    }
                }
            }
        }
    }
}
