﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travail
{
    class Program
    {

        public static int LeMenu()
        {
            //Menu
            Console.WriteLine("######## Bienvenue dans la plateforme des jeux. Voici le menu ########");
            Console.WriteLine("**** 1 - Lister les Jeux ********");
            Console.WriteLine("**** 2 - Lister les Jeux Retro ********");
            Console.WriteLine("**** 3 - Trier la liste des Jeux ********");
            Console.WriteLine("**** 4 - Ajouter un Jeu ********");
            Console.WriteLine("**** 5 - Rechercher un jeu ********");
            Console.WriteLine("**** 6 - Supprimer Jeu ********");
            Console.WriteLine("**** 7 - Comparer Jeu ********");
            Console.WriteLine("**** 8 - Ajouter Jeu dans votre panier ********");
            Console.WriteLine("**** 9 - Trier votre panier ********");
            Console.WriteLine("**** 10 - Inserer objet à un indice de votre panier ********");
            Console.WriteLine("**** 11 - Quitter ********");
            string claviers;
            int clavier = 12;
            Boolean clap = false, saisie = false;
            do
            {
                clap = false;
                saisie = false;
                Console.WriteLine("######### Vous pouvez choisir votre option ##########");
                //saisie
                try
                {
                    claviers = Console.ReadLine();
                    clavier = Convert.ToInt32(claviers);
                    if (clavier > 11 || clavier < 1) { Console.WriteLine("Vous devez choisir en fontion du menu"); saisie = true; clap = true; }
                }
                catch
                {
                    Console.WriteLine("Vous n'avez pas saisi un nombre entier");
                    clap = true;
                    saisie = true;
                }
            } while (clap && saisie);
            return clavier;
        }
        static void Main(string[] args)
        {
            //date actuel
            DateTime dateJ = DateTime.Now;
            //instanciation de l'objet LesJeux
            LesJeux lesjeux = new LesJeux();
            JeuRetro jeux = new JeuRetro("TafTaf", "TafTAf. est un jeu d'arcade développé et édité par Nintendo en 1983", "Shigeru Miyamoto", "Plateforme", Categorie.Course, 9, dateJ, true, null,"Bon",true,null);
            JeuRetro jeux1 = new JeuRetro("Damon X", "Dragon Ball Z est de combat ", "Azerty", "Ordinateur", Categorie.Action, 17, dateJ, true, null,"Tres Bon",true,null);
            
            //Instnciation de quelques objets Jeu
            Jeu jeu1 = new Jeu("Mario", "Mario Bros. est un jeu d'arcade développé et édité par Nintendo en 1983", "Shigeru Miyamoto","Plateforme",Categorie.Course,9,dateJ,true,null);
            Jeu jeu2 = new Jeu("Dragon Ball Z", "Dragon Ball Z est de combat ", "Azerty", "Ordinateur", Categorie.Action, 17, dateJ, true, null);
            Jeu jeu3 = new Jeu("GTA", "Grand Therf Auto. est un jeu de combat, de mission", "Moimeme", "Plateforme", Categorie.Autres, 54, dateJ, true, null);
            Jeu jeu4 = new Jeu("GTA1", "Grand Therf Auto. est un jeu de combats, de missionf", "Moimemef", "Plateformet", Categorie.Autres, 50, dateJ, true, null);
            //Ajout des jeux
            lesjeux.AjoutJeu(jeu1);
            lesjeux.AjoutJeu(jeu2);
            lesjeux.AjoutJeu(jeu3);
            lesjeux.AjoutJeuRetro(jeux);
            lesjeux.AjoutJeuRetro(jeux1);

            //Instance Panier de jeu
            Panier<Jeu> panier = new Panier<Jeu>();
            Boolean repete = true;
            do
            {

                int clavier = LeMenu();
            
                //boucle pour choisir la tache à executer
                switch (clavier)
                {
                    case 1:
                        //lister tous les jeux
                        Console.WriteLine(lesjeux.ToString());
                        break;
                    case 2:
                        //liste des Jeux Retro
                        Console.WriteLine(lesjeux.ToStrings());
                        break;
                    case 3:
                        Console.WriteLine("############### Avant trie par Nom #############");
                        Console.WriteLine(lesjeux.ToString());
                        Console.WriteLine("############### Après trie par Nom #############");
                        lesjeux.Trier();
                        Console.WriteLine(lesjeux.ToString());
                        break;
                    case 4:
                        Jeu jeu = new Jeu();
                        jeu.Saisie();
                        lesjeux.AjoutJeu(jeu);
                        Console.WriteLine("######### Liste des Jeux ##########");
                        Console.WriteLine(lesjeux.ToString());
                        break;

                    case 5:
                        Console.WriteLine("###### Recherche par indice ou nom? Tapez i pour indice n pour nom ######");
                        string res=Console.ReadLine();
                        Jeu result;
                        if (res == "i")
                        {
                            string rs;
                            do
                            {
                                Console.WriteLine("donner l'indice, un entier");
                                 rs = Console.ReadLine();

                            } while (Convert.ToInt32(rs) <0 );
                        
                            result=lesjeux.GetJeuIndice(Convert.ToInt32(rs));
                        }
                        else
                        {
                            Console.WriteLine("donner le nom");
                            string rs = Console.ReadLine();

                            result=lesjeux.GetJeuNom(rs);
                        }
                        if (result != null) { Console.WriteLine(result.ToString()); }
                        else { Console.WriteLine("L'argument donnée en paramètre est non reconnu"); }
                        break;
                    case 6:
                        Console.WriteLine("Donner le nom du jeu à supprimer");
                        string nom = Console.ReadLine();
                        lesjeux.Supprime(nom);
                        Console.WriteLine(lesjeux.ToString());
                        break;
                    case 7:
                        Console.WriteLine("Donner le nom du 1er jeu");
                        string pan3 = Console.ReadLine();
                        Jeu jeuPan3 = lesjeux.GetJeuNom(pan3);
                        Console.WriteLine("Donner le nom du second jeu");
                        string pan4 = Console.ReadLine();
                        Jeu jeuPan4 = lesjeux.GetJeuNom(pan4);
                        try
                        {
                            int trouve = jeuPan3.CompareTo(jeuPan4);
                            if (jeuPan3 != null && jeuPan4 != null) { Console.WriteLine(); }
                            else { Console.WriteLine("nom entrée n'existe pas dans la base"); }
                            if (trouve == 0) { Console.WriteLine("Les deux Jeux que vous comparer ont le même prix"); }
                            else
                            {
                                if (trouve > 0) { Console.WriteLine("Le prix de " + jeuPan3.Nom + " est supérieur de " +(jeuPan3.Prix - jeuPan4.Prix) + "£ par rapport à " + jeuPan4.Nom); }
                                else { Console.WriteLine("Le prix de " + jeuPan3.Nom + " est inférieur de " + (jeuPan4.Prix - jeuPan3.Prix) + "£ par rapport à " + jeuPan4.Nom); }
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Le nom du Jeu n'est pas dans la base");
                        }
                        break;
                    case 8:
                        Console.WriteLine("Donner le nom du jeu à ajouter dans ton panier");
                        string pan1 = Console.ReadLine();
                        Jeu jeuPan= lesjeux.GetJeuNom(pan1);
                        if (jeuPan != null) { panier.Ajout(jeuPan); Console.WriteLine("Panier Ajouter! Voici la liste"); panier.Affiche() ; }
                        else { Console.WriteLine("L'argument donnée en paramètre est non reconnu"); }
                        break;
                    case 9:
                        Console.WriteLine("Avant Trie du Panier");
                        panier.Affiche();
                        Console.WriteLine("Après trie du Panier");
                        panier.Tri();
                        panier.Affiche();
                        break;
                    case 10:
                        Console.WriteLine("Donner le nom du jeu à ajouter dans ton panier");
                        string pan2 = Console.ReadLine();
                        string ind="";
                        Boolean test = false;
                        do
                        {
                            Console.WriteLine("Donner l'indice de sa position");
                            try
                            {
                                ind = Console.ReadLine();
                                Convert.ToInt32(ind);
                            
                            }
                            catch
                            {
                                Console.WriteLine("Vous n'avez pas saisi un nombre entier");
                                test = true;
                            }
                        } while (test);
                        Jeu ins = lesjeux.GetJeuNom(pan2);
                        if (ins != null) { Console.WriteLine(ins.ToString()); }
                        else { Console.WriteLine("Le nom entrée n'existe pas dans la base"); }
                        panier.Insere(ins, Convert.ToInt32(ind));
                        panier.Affiche();
                        break;
                    case 11:
                        repete = false;
                        break;
                    default:
                        Console.WriteLine("Désolé Bye");
                        repete = false;
                        break;
                }
                Console.WriteLine();
            } while (repete);
        }
    }
}
