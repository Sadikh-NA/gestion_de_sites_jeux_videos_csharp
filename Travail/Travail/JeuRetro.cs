﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travail
{
    //Classe JeuRetro qui hérite de la classe jeu
    public class JeuRetro : Jeu
    {

        //attribut nouvelle (car il a tous les attributs que Jeu)
        private string etat;
        private List<Image> lphotos;
        private Boolean notice;

        //constructeur par défaut
        public JeuRetro()
        {
            this.Etat = "";
            this.Notice = false;
            this.Lphotos = null;
        }
        //Constructeur avec parametre
        public JeuRetro(string n, string d, string e, string pl, Categorie c, float p, DateTime dte, Boolean r, Image ph, string etat, Boolean notice, List<Image> lphotos):base(n,d,e,pl,c,p,dte,r,ph)
        {
            this.Etat = etat;
            this.Notice = notice;
            this.Lphotos = new List<Image>();
        }

        //Accesseurs et mutateurs des attributs
        public string Etat { get => etat; set => etat = value; }
        public List<Image> Lphotos { get => lphotos; set => lphotos = value; }
        public bool Notice { get => notice; set => notice = value; }


        //surcharge de la classe ToString
        public override string ToString()
        {
            Jeu jeu = new Jeu(Nom, Description, Editeur, Plateforme, Genre, Prix, DateSortie, Disponibilite, Photo);
            string s = jeu.ToString();
            s += "\n etat: " + this.etat;
            s += "\n Notice: " + this.notice;
            return s;
        }

        //
        public void Saisie()
        {
            Jeu jeu = new JeuRetro();
            this.notice = false;
            Console.WriteLine("Etat");
            this.etat = Console.ReadLine();
            Console.WriteLine("Notice: Vrai/Faux");
            string s = Console.ReadLine();
            if (s == "Vrai") { this.notice = true; }
            jeu.Saisie();

        }

        //methode d'affichage du ToString
        public void afficher()
        {
            Console.WriteLine(this.ToString());
        }
    }
}
