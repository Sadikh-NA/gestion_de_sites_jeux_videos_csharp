﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TPCDAAA
{
    public partial class AddJeux : Form
    {
        private JeuRetro leJeuR;
        private Jeu leJeu;
        private Catalogue leCat;
        private List<Image> lph;
        private Image photo1, photo2, photo3, photo, photo4;
        private TabControl tabContrel1;
        private TabPage SaisieJeu;
        private TableLayoutPanel tableLayoutPanel3;
        private Label LNom;
        private TextBox Nom;
        private Label LDescription;
        private TextBox Description;
        private Label LPlateforme;
        private TextBox Plateforme;
        private Label LEditeur;
        private TextBox Editeur;
        private GroupBox groupBoxRecond;
        private RadioButton RBNRecond;
        private RadioButton RBRecond;
        private Button BAnnuler;
        private TabPage SaisieJeuRetro;
        private Label LGenre;
        private ComboBox ListeGenres;
        private Label LPrix;
        private TextBox Prix;
        private Label LDate;
        private DateTimePicker dateTimePicker1;
        private Label Photo;
        private Button BParcourir;
        private TextBox NomFich;
        private Button BValider;
        private TableLayoutPanel tableLayoutPanel1;
        private Label LNomR;
        private TextBox NomR;
        private Label LDescriptionR;
        private TextBox DescriptionR;
        private Label LPlateformeR;
        private TextBox PlateformeR;
        private Label LEditeurR;
        private TextBox EditeurR;
        private Label LGenreR;
        private ComboBox ListeGenresR;
        private Label LPrixR;
        private TextBox PrixR;
        private Label LDateR;
        private DateTimePicker dateTimePicker2;
        private Label PhotoR;
        private Button BParcourirR;
        private TextBox NomFichR;
        private PictureBox PBPhotoR;
        private Button BValiderR;
        private Button BAnnulerR;
        private GroupBox groupBoxNotice;
        private RadioButton RBNNotice;
        private RadioButton RBNotice;
        private Button BParcourirR1;
        private TextBox NomFichR1;
        private PictureBox PBPhotoR1;
        private Button BParcourirR2;
        private TextBox NomFichR2;
        private PictureBox PBPhotoR2;
        private Button BParcourir3;
        private TextBox NomFichR3;
        private PictureBox PBPhotoR3;
        private Label LEtat;
        private ComboBox Etat;
        private PictureBox PBPhoto;

        public AddJeux()
        {
            InitializeComponent();
            this.LeJeu = new Jeu();
            this.LeJeuR = new JeuRetro();
            this.photo = null;
            this.photo1 = null;
            this.photo2 = null;
            this.photo3 = null;
            this.photo4 = null;
            this.lph = new List<Image>();
            initialiseGenre();
        }

        public Jeu LeJeu { get => leJeu; set => leJeu = value; }
        //public Form1 Formule { get => formule; set => formule = value; }

        public Catalogue LeCat { get => leCat; set => leCat = value; }
        public JeuRetro LeJeuR { get => leJeuR; set => leJeuR = value; }

        private void InitializeComponent()
        {
            this.tabContrel1 = new System.Windows.Forms.TabControl();
            this.SaisieJeu = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.LNom = new System.Windows.Forms.Label();
            this.Nom = new System.Windows.Forms.TextBox();
            this.LDescription = new System.Windows.Forms.Label();
            this.Description = new System.Windows.Forms.TextBox();
            this.LPlateforme = new System.Windows.Forms.Label();
            this.Plateforme = new System.Windows.Forms.TextBox();
            this.LEditeur = new System.Windows.Forms.Label();
            this.Editeur = new System.Windows.Forms.TextBox();
            this.groupBoxRecond = new System.Windows.Forms.GroupBox();
            this.RBNRecond = new System.Windows.Forms.RadioButton();
            this.RBRecond = new System.Windows.Forms.RadioButton();
            this.BAnnuler = new System.Windows.Forms.Button();
            this.LGenre = new System.Windows.Forms.Label();
            this.ListeGenres = new System.Windows.Forms.ComboBox();
            this.LPrix = new System.Windows.Forms.Label();
            this.Prix = new System.Windows.Forms.TextBox();
            this.LDate = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Photo = new System.Windows.Forms.Label();
            this.BParcourir = new System.Windows.Forms.Button();
            this.NomFich = new System.Windows.Forms.TextBox();
            this.PBPhoto = new System.Windows.Forms.PictureBox();
            this.BValider = new System.Windows.Forms.Button();
            this.SaisieJeuRetro = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LNomR = new System.Windows.Forms.Label();
            this.NomR = new System.Windows.Forms.TextBox();
            this.LDescriptionR = new System.Windows.Forms.Label();
            this.DescriptionR = new System.Windows.Forms.TextBox();
            this.LPlateformeR = new System.Windows.Forms.Label();
            this.PlateformeR = new System.Windows.Forms.TextBox();
            this.LEditeurR = new System.Windows.Forms.Label();
            this.EditeurR = new System.Windows.Forms.TextBox();
            this.LGenreR = new System.Windows.Forms.Label();
            this.ListeGenresR = new System.Windows.Forms.ComboBox();
            this.LPrixR = new System.Windows.Forms.Label();
            this.PrixR = new System.Windows.Forms.TextBox();
            this.LDateR = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.PhotoR = new System.Windows.Forms.Label();
            this.BParcourirR = new System.Windows.Forms.Button();
            this.NomFichR = new System.Windows.Forms.TextBox();
            this.PBPhotoR = new System.Windows.Forms.PictureBox();
            this.BValiderR = new System.Windows.Forms.Button();
            this.BAnnulerR = new System.Windows.Forms.Button();
            this.groupBoxNotice = new System.Windows.Forms.GroupBox();
            this.RBNNotice = new System.Windows.Forms.RadioButton();
            this.RBNotice = new System.Windows.Forms.RadioButton();
            this.BParcourirR1 = new System.Windows.Forms.Button();
            this.NomFichR1 = new System.Windows.Forms.TextBox();
            this.PBPhotoR1 = new System.Windows.Forms.PictureBox();
            this.BParcourirR2 = new System.Windows.Forms.Button();
            this.NomFichR2 = new System.Windows.Forms.TextBox();
            this.PBPhotoR2 = new System.Windows.Forms.PictureBox();
            this.BParcourir3 = new System.Windows.Forms.Button();
            this.NomFichR3 = new System.Windows.Forms.TextBox();
            this.PBPhotoR3 = new System.Windows.Forms.PictureBox();
            this.LEtat = new System.Windows.Forms.Label();
            this.Etat = new System.Windows.Forms.ComboBox();
            this.tabContrel1.SuspendLayout();
            this.SaisieJeu.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBoxRecond.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhoto)).BeginInit();
            this.SaisieJeuRetro.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR)).BeginInit();
            this.groupBoxNotice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabContrel1
            // 
            this.tabContrel1.Controls.Add(this.SaisieJeu);
            this.tabContrel1.Controls.Add(this.SaisieJeuRetro);
            this.tabContrel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContrel1.Location = new System.Drawing.Point(0, 0);
            this.tabContrel1.Name = "tabContrel1";
            this.tabContrel1.SelectedIndex = 0;
            this.tabContrel1.Size = new System.Drawing.Size(932, 450);
            this.tabContrel1.TabIndex = 2;
            // 
            // SaisieJeu
            // 
            this.SaisieJeu.Controls.Add(this.tableLayoutPanel3);
            this.SaisieJeu.Location = new System.Drawing.Point(4, 22);
            this.SaisieJeu.Name = "SaisieJeu";
            this.SaisieJeu.Size = new System.Drawing.Size(924, 424);
            this.SaisieJeu.TabIndex = 0;
            this.SaisieJeu.Text = "SaisieJeu";
            this.SaisieJeu.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 193F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 201F));
            this.tableLayoutPanel3.Controls.Add(this.LNom, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Nom, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.LDescription, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Description, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.LPlateforme, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.Plateforme, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.LEditeur, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.Editeur, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.groupBoxRecond, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.BAnnuler, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.LGenre, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.ListeGenres, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.LPrix, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.Prix, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.LDate, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.dateTimePicker1, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.Photo, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.BParcourir, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.NomFich, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.PBPhoto, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.BValider, 2, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.38554F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.61446F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(924, 424);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // LNom
            // 
            this.LNom.AutoSize = true;
            this.LNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LNom.Location = new System.Drawing.Point(3, 0);
            this.LNom.Name = "LNom";
            this.LNom.Size = new System.Drawing.Size(259, 72);
            this.LNom.TabIndex = 0;
            this.LNom.Text = "Nom?";
            this.LNom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Nom
            // 
            this.Nom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Nom.Location = new System.Drawing.Point(268, 3);
            this.Nom.Multiline = true;
            this.Nom.Name = "Nom";
            this.Nom.Size = new System.Drawing.Size(259, 66);
            this.Nom.TabIndex = 1;
            // 
            // LDescription
            // 
            this.LDescription.AutoSize = true;
            this.LDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LDescription.Location = new System.Drawing.Point(3, 72);
            this.LDescription.Name = "LDescription";
            this.LDescription.Size = new System.Drawing.Size(259, 84);
            this.LDescription.TabIndex = 2;
            this.LDescription.Text = "Description";
            this.LDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Description
            // 
            this.Description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Description.Location = new System.Drawing.Point(268, 75);
            this.Description.Multiline = true;
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(259, 78);
            this.Description.TabIndex = 3;
            // 
            // LPlateforme
            // 
            this.LPlateforme.AutoSize = true;
            this.LPlateforme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LPlateforme.Location = new System.Drawing.Point(3, 156);
            this.LPlateforme.Name = "LPlateforme";
            this.LPlateforme.Size = new System.Drawing.Size(259, 72);
            this.LPlateforme.TabIndex = 4;
            this.LPlateforme.Text = "Plateforme ?";
            this.LPlateforme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Plateforme
            // 
            this.Plateforme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Plateforme.Location = new System.Drawing.Point(268, 159);
            this.Plateforme.Multiline = true;
            this.Plateforme.Name = "Plateforme";
            this.Plateforme.Size = new System.Drawing.Size(259, 66);
            this.Plateforme.TabIndex = 5;
            // 
            // LEditeur
            // 
            this.LEditeur.AutoSize = true;
            this.LEditeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LEditeur.Location = new System.Drawing.Point(3, 228);
            this.LEditeur.Name = "LEditeur";
            this.LEditeur.Size = new System.Drawing.Size(259, 67);
            this.LEditeur.TabIndex = 6;
            this.LEditeur.Text = "Editeur ?";
            this.LEditeur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Editeur
            // 
            this.Editeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Editeur.Location = new System.Drawing.Point(268, 231);
            this.Editeur.Multiline = true;
            this.Editeur.Name = "Editeur";
            this.Editeur.Size = new System.Drawing.Size(259, 61);
            this.Editeur.TabIndex = 7;
            // 
            // groupBoxRecond
            // 
            this.groupBoxRecond.Controls.Add(this.RBNRecond);
            this.groupBoxRecond.Controls.Add(this.RBRecond);
            this.groupBoxRecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxRecond.Location = new System.Drawing.Point(268, 298);
            this.groupBoxRecond.Name = "groupBoxRecond";
            this.groupBoxRecond.Size = new System.Drawing.Size(259, 65);
            this.groupBoxRecond.TabIndex = 8;
            this.groupBoxRecond.TabStop = false;
            // 
            // RBNRecond
            // 
            this.RBNRecond.AutoSize = true;
            this.RBNRecond.Location = new System.Drawing.Point(0, 38);
            this.RBNRecond.Name = "RBNRecond";
            this.RBNRecond.Size = new System.Drawing.Size(117, 17);
            this.RBNRecond.TabIndex = 1;
            this.RBNRecond.TabStop = true;
            this.RBNRecond.Text = "Non Reconditionné";
            this.RBNRecond.UseVisualStyleBackColor = true;
            // 
            // RBRecond
            // 
            this.RBRecond.AutoSize = true;
            this.RBRecond.Location = new System.Drawing.Point(0, 15);
            this.RBRecond.Name = "RBRecond";
            this.RBRecond.Size = new System.Drawing.Size(94, 17);
            this.RBRecond.TabIndex = 0;
            this.RBRecond.TabStop = true;
            this.RBRecond.Text = "Reconditionné";
            this.RBRecond.UseVisualStyleBackColor = true;
            // 
            // BAnnuler
            // 
            this.BAnnuler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BAnnuler.Location = new System.Drawing.Point(268, 369);
            this.BAnnuler.Name = "BAnnuler";
            this.BAnnuler.Size = new System.Drawing.Size(259, 52);
            this.BAnnuler.TabIndex = 9;
            this.BAnnuler.Text = "Annuler";
            this.BAnnuler.UseVisualStyleBackColor = true;
            this.BAnnuler.Click += new System.EventHandler(this.BAnnuler_click);
            // 
            // LGenre
            // 
            this.LGenre.AutoSize = true;
            this.LGenre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LGenre.Location = new System.Drawing.Point(533, 0);
            this.LGenre.Name = "LGenre";
            this.LGenre.Size = new System.Drawing.Size(187, 72);
            this.LGenre.TabIndex = 10;
            this.LGenre.Text = "Genre ?";
            this.LGenre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ListeGenres
            // 
            this.ListeGenres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListeGenres.FormattingEnabled = true;
            this.ListeGenres.Location = new System.Drawing.Point(726, 3);
            this.ListeGenres.Name = "ListeGenres";
            this.ListeGenres.Size = new System.Drawing.Size(195, 21);
            this.ListeGenres.TabIndex = 11;
            // 
            // LPrix
            // 
            this.LPrix.AutoSize = true;
            this.LPrix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LPrix.Location = new System.Drawing.Point(533, 72);
            this.LPrix.Name = "LPrix";
            this.LPrix.Size = new System.Drawing.Size(187, 84);
            this.LPrix.TabIndex = 12;
            this.LPrix.Text = "Prix ?";
            this.LPrix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Prix
            // 
            this.Prix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Prix.Location = new System.Drawing.Point(726, 75);
            this.Prix.Multiline = true;
            this.Prix.Name = "Prix";
            this.Prix.Size = new System.Drawing.Size(195, 78);
            this.Prix.TabIndex = 13;
            // 
            // LDate
            // 
            this.LDate.AutoSize = true;
            this.LDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LDate.Location = new System.Drawing.Point(533, 156);
            this.LDate.Name = "LDate";
            this.LDate.Size = new System.Drawing.Size(187, 72);
            this.LDate.TabIndex = 14;
            this.LDate.Text = "Date de Parution";
            this.LDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker1.Location = new System.Drawing.Point(726, 159);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 20);
            this.dateTimePicker1.TabIndex = 15;
            // 
            // Photo
            // 
            this.Photo.AutoSize = true;
            this.Photo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Photo.Location = new System.Drawing.Point(533, 228);
            this.Photo.Name = "Photo";
            this.Photo.Size = new System.Drawing.Size(187, 67);
            this.Photo.TabIndex = 16;
            this.Photo.Text = "Photo ?";
            this.Photo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BParcourir
            // 
            this.BParcourir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BParcourir.Location = new System.Drawing.Point(726, 231);
            this.BParcourir.Name = "BParcourir";
            this.BParcourir.Size = new System.Drawing.Size(195, 61);
            this.BParcourir.TabIndex = 17;
            this.BParcourir.Text = "Parcourir";
            this.BParcourir.UseVisualStyleBackColor = true;
            this.BParcourir.Click += new System.EventHandler(this.BParcourir_click);
            // 
            // NomFich
            // 
            this.NomFich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomFich.Location = new System.Drawing.Point(533, 298);
            this.NomFich.Multiline = true;
            this.NomFich.Name = "NomFich";
            this.NomFich.ReadOnly = true;
            this.NomFich.Size = new System.Drawing.Size(187, 65);
            this.NomFich.TabIndex = 18;
            this.NomFich.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PBPhoto
            // 
            this.PBPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhoto.Location = new System.Drawing.Point(726, 298);
            this.PBPhoto.Name = "PBPhoto";
            this.PBPhoto.Size = new System.Drawing.Size(195, 65);
            this.PBPhoto.TabIndex = 19;
            this.PBPhoto.TabStop = false;
            // 
            // BValider
            // 
            this.BValider.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BValider.Location = new System.Drawing.Point(533, 369);
            this.BValider.Name = "BValider";
            this.BValider.Size = new System.Drawing.Size(187, 52);
            this.BValider.TabIndex = 20;
            this.BValider.Text = "Valider";
            this.BValider.UseVisualStyleBackColor = true;
            this.BValider.Click += new System.EventHandler(this.BValider_Click);
            // 
            // SaisieJeuRetro
            // 
            this.SaisieJeuRetro.Controls.Add(this.tableLayoutPanel1);
            this.SaisieJeuRetro.Location = new System.Drawing.Point(4, 22);
            this.SaisieJeuRetro.Name = "SaisieJeuRetro";
            this.SaisieJeuRetro.Size = new System.Drawing.Size(924, 424);
            this.SaisieJeuRetro.TabIndex = 0;
            this.SaisieJeuRetro.Text = "SaisieJeuRetro";
            this.SaisieJeuRetro.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.13355F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.86645F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel1.Controls.Add(this.LNomR, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.NomR, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.LDescriptionR, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.DescriptionR, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.LPlateformeR, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.PlateformeR, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.LEditeurR, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.EditeurR, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.LGenreR, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ListeGenresR, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.LPrixR, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.PrixR, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.LDateR, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.PhotoR, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.BParcourirR, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.NomFichR, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.PBPhotoR, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.BValiderR, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.BAnnulerR, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxNotice, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.BParcourirR1, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.NomFichR1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.PBPhotoR1, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.BParcourirR2, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.NomFichR2, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.PBPhotoR2, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.BParcourir3, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.NomFichR3, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.PBPhotoR3, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.LEtat, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.Etat, 5, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.125F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.875F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(924, 424);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // LNomR
            // 
            this.LNomR.AccessibleName = "LNom";
            this.LNomR.AutoSize = true;
            this.LNomR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LNomR.Location = new System.Drawing.Point(3, 0);
            this.LNomR.Name = "LNomR";
            this.LNomR.Size = new System.Drawing.Size(100, 56);
            this.LNomR.TabIndex = 0;
            this.LNomR.Text = "Nom";
            this.LNomR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NomR
            // 
            this.NomR.AccessibleName = "Nom";
            this.NomR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomR.Location = new System.Drawing.Point(109, 3);
            this.NomR.Multiline = true;
            this.NomR.Name = "NomR";
            this.NomR.Size = new System.Drawing.Size(175, 50);
            this.NomR.TabIndex = 1;
            // 
            // LDescriptionR
            // 
            this.LDescriptionR.AccessibleName = "LDescriptions";
            this.LDescriptionR.AutoSize = true;
            this.LDescriptionR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LDescriptionR.Location = new System.Drawing.Point(3, 56);
            this.LDescriptionR.Name = "LDescriptionR";
            this.LDescriptionR.Size = new System.Drawing.Size(100, 74);
            this.LDescriptionR.TabIndex = 2;
            this.LDescriptionR.Text = "Description";
            this.LDescriptionR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DescriptionR
            // 
            this.DescriptionR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DescriptionR.Location = new System.Drawing.Point(109, 59);
            this.DescriptionR.Multiline = true;
            this.DescriptionR.Name = "DescriptionR";
            this.DescriptionR.Size = new System.Drawing.Size(175, 68);
            this.DescriptionR.TabIndex = 3;
            // 
            // LPlateformeR
            // 
            this.LPlateformeR.AutoSize = true;
            this.LPlateformeR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LPlateformeR.Location = new System.Drawing.Point(3, 130);
            this.LPlateformeR.Name = "LPlateformeR";
            this.LPlateformeR.Size = new System.Drawing.Size(100, 74);
            this.LPlateformeR.TabIndex = 4;
            this.LPlateformeR.Text = "Plateforme";
            this.LPlateformeR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PlateformeR
            // 
            this.PlateformeR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PlateformeR.Location = new System.Drawing.Point(109, 133);
            this.PlateformeR.Multiline = true;
            this.PlateformeR.Name = "PlateformeR";
            this.PlateformeR.Size = new System.Drawing.Size(175, 68);
            this.PlateformeR.TabIndex = 5;
            // 
            // LEditeurR
            // 
            this.LEditeurR.AutoSize = true;
            this.LEditeurR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LEditeurR.Location = new System.Drawing.Point(3, 204);
            this.LEditeurR.Name = "LEditeurR";
            this.LEditeurR.Size = new System.Drawing.Size(100, 74);
            this.LEditeurR.TabIndex = 6;
            this.LEditeurR.Text = "Editeur";
            this.LEditeurR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditeurR
            // 
            this.EditeurR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditeurR.Location = new System.Drawing.Point(109, 207);
            this.EditeurR.Multiline = true;
            this.EditeurR.Name = "EditeurR";
            this.EditeurR.Size = new System.Drawing.Size(175, 68);
            this.EditeurR.TabIndex = 7;
            // 
            // LGenreR
            // 
            this.LGenreR.AutoSize = true;
            this.LGenreR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LGenreR.Location = new System.Drawing.Point(290, 0);
            this.LGenreR.Name = "LGenreR";
            this.LGenreR.Size = new System.Drawing.Size(123, 56);
            this.LGenreR.TabIndex = 8;
            this.LGenreR.Text = "Genre";
            this.LGenreR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ListeGenresR
            // 
            this.ListeGenresR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListeGenresR.FormattingEnabled = true;
            this.ListeGenresR.Items.AddRange(new object[] {
            "Course",
            "Action",
            "Aventure",
            "Autres"});
            this.ListeGenresR.Location = new System.Drawing.Point(419, 3);
            this.ListeGenresR.Name = "ListeGenresR";
            this.ListeGenresR.Size = new System.Drawing.Size(179, 21);
            this.ListeGenresR.TabIndex = 9;
            // 
            // LPrixR
            // 
            this.LPrixR.AutoSize = true;
            this.LPrixR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LPrixR.Location = new System.Drawing.Point(290, 56);
            this.LPrixR.Name = "LPrixR";
            this.LPrixR.Size = new System.Drawing.Size(123, 74);
            this.LPrixR.TabIndex = 10;
            this.LPrixR.Text = "Prix";
            this.LPrixR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PrixR
            // 
            this.PrixR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrixR.Location = new System.Drawing.Point(419, 59);
            this.PrixR.Multiline = true;
            this.PrixR.Name = "PrixR";
            this.PrixR.Size = new System.Drawing.Size(179, 68);
            this.PrixR.TabIndex = 11;
            // 
            // LDateR
            // 
            this.LDateR.AutoSize = true;
            this.LDateR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LDateR.Location = new System.Drawing.Point(290, 130);
            this.LDateR.Name = "LDateR";
            this.LDateR.Size = new System.Drawing.Size(123, 74);
            this.LDateR.TabIndex = 12;
            this.LDateR.Text = "Date de Sortie";
            this.LDateR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker2.Location = new System.Drawing.Point(419, 133);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(179, 20);
            this.dateTimePicker2.TabIndex = 13;
            // 
            // PhotoR
            // 
            this.PhotoR.AutoSize = true;
            this.PhotoR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PhotoR.Location = new System.Drawing.Point(290, 204);
            this.PhotoR.Name = "PhotoR";
            this.PhotoR.Size = new System.Drawing.Size(123, 74);
            this.PhotoR.TabIndex = 14;
            this.PhotoR.Text = "Image";
            this.PhotoR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BParcourirR
            // 
            this.BParcourirR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BParcourirR.Location = new System.Drawing.Point(419, 207);
            this.BParcourirR.Name = "BParcourirR";
            this.BParcourirR.Size = new System.Drawing.Size(179, 68);
            this.BParcourirR.TabIndex = 15;
            this.BParcourirR.Text = "Parcourir";
            this.BParcourirR.UseVisualStyleBackColor = true;
            this.BParcourirR.Click += new System.EventHandler(this.BParcourirR_click);
            // 
            // NomFichR
            // 
            this.NomFichR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomFichR.Location = new System.Drawing.Point(290, 281);
            this.NomFichR.Multiline = true;
            this.NomFichR.Name = "NomFichR";
            this.NomFichR.ReadOnly = true;
            this.NomFichR.Size = new System.Drawing.Size(123, 65);
            this.NomFichR.TabIndex = 16;
            // 
            // PBPhotoR
            // 
            this.PBPhotoR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhotoR.Location = new System.Drawing.Point(419, 281);
            this.PBPhotoR.Name = "PBPhotoR";
            this.PBPhotoR.Size = new System.Drawing.Size(179, 65);
            this.PBPhotoR.TabIndex = 17;
            this.PBPhotoR.TabStop = false;
            // 
            // BValiderR
            // 
            this.BValiderR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BValiderR.Location = new System.Drawing.Point(419, 352);
            this.BValiderR.Name = "BValiderR";
            this.BValiderR.Size = new System.Drawing.Size(179, 69);
            this.BValiderR.TabIndex = 18;
            this.BValiderR.Text = "Valider";
            this.BValiderR.UseVisualStyleBackColor = true;
            this.BValiderR.Click += new System.EventHandler(this.BValiderR_Click);
            // 
            // BAnnulerR
            // 
            this.BAnnulerR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BAnnulerR.Location = new System.Drawing.Point(290, 352);
            this.BAnnulerR.Name = "BAnnulerR";
            this.BAnnulerR.Size = new System.Drawing.Size(123, 69);
            this.BAnnulerR.TabIndex = 19;
            this.BAnnulerR.Text = "Annuler";
            this.BAnnulerR.UseVisualStyleBackColor = true;
            this.BAnnulerR.Click += new System.EventHandler(this.BAnnuler_click);
            // 
            // groupBoxNotice
            // 
            this.groupBoxNotice.Controls.Add(this.RBNNotice);
            this.groupBoxNotice.Controls.Add(this.RBNotice);
            this.groupBoxNotice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxNotice.Location = new System.Drawing.Point(109, 281);
            this.groupBoxNotice.Name = "groupBoxNotice";
            this.groupBoxNotice.Size = new System.Drawing.Size(175, 65);
            this.groupBoxNotice.TabIndex = 20;
            this.groupBoxNotice.TabStop = false;
            // 
            // RBNNotice
            // 
            this.RBNNotice.AutoSize = true;
            this.RBNNotice.Dock = System.Windows.Forms.DockStyle.Top;
            this.RBNNotice.Location = new System.Drawing.Point(3, 33);
            this.RBNNotice.Name = "RBNNotice";
            this.RBNNotice.Size = new System.Drawing.Size(169, 17);
            this.RBNNotice.TabIndex = 1;
            this.RBNNotice.TabStop = true;
            this.RBNNotice.Text = "Non Notice";
            this.RBNNotice.UseVisualStyleBackColor = true;
            // 
            // RBNotice
            // 
            this.RBNotice.AutoSize = true;
            this.RBNotice.Dock = System.Windows.Forms.DockStyle.Top;
            this.RBNotice.Location = new System.Drawing.Point(3, 16);
            this.RBNotice.Name = "RBNotice";
            this.RBNotice.Size = new System.Drawing.Size(169, 17);
            this.RBNotice.TabIndex = 0;
            this.RBNotice.TabStop = true;
            this.RBNotice.Text = "Notice";
            this.RBNotice.UseVisualStyleBackColor = true;
            // 
            // BParcourirR1
            // 
            this.BParcourirR1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BParcourirR1.Location = new System.Drawing.Point(843, 3);
            this.BParcourirR1.Name = "BParcourirR1";
            this.BParcourirR1.Size = new System.Drawing.Size(78, 50);
            this.BParcourirR1.TabIndex = 21;
            this.BParcourirR1.Text = "Parcourir";
            this.BParcourirR1.UseVisualStyleBackColor = true;
            this.BParcourirR1.Click += new System.EventHandler(this.BParcourirR1_click);
            // 
            // NomFichR1
            // 
            this.NomFichR1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomFichR1.Location = new System.Drawing.Point(604, 3);
            this.NomFichR1.Multiline = true;
            this.NomFichR1.Name = "NomFichR1";
            this.NomFichR1.ReadOnly = true;
            this.NomFichR1.Size = new System.Drawing.Size(117, 50);
            this.NomFichR1.TabIndex = 22;
            // 
            // PBPhotoR1
            // 
            this.PBPhotoR1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhotoR1.Location = new System.Drawing.Point(727, 3);
            this.PBPhotoR1.Name = "PBPhotoR1";
            this.PBPhotoR1.Size = new System.Drawing.Size(110, 50);
            this.PBPhotoR1.TabIndex = 23;
            this.PBPhotoR1.TabStop = false;
            // 
            // BParcourirR2
            // 
            this.BParcourirR2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BParcourirR2.Location = new System.Drawing.Point(843, 59);
            this.BParcourirR2.Name = "BParcourirR2";
            this.BParcourirR2.Size = new System.Drawing.Size(78, 68);
            this.BParcourirR2.TabIndex = 21;
            this.BParcourirR2.Text = "Parcourir";
            this.BParcourirR2.UseVisualStyleBackColor = true;
            this.BParcourirR2.Click += new System.EventHandler(this.BParcourirR2_click);
            // 
            // NomFichR2
            // 
            this.NomFichR2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomFichR2.Location = new System.Drawing.Point(604, 59);
            this.NomFichR2.Multiline = true;
            this.NomFichR2.Name = "NomFichR2";
            this.NomFichR2.ReadOnly = true;
            this.NomFichR2.Size = new System.Drawing.Size(117, 68);
            this.NomFichR2.TabIndex = 22;
            // 
            // PBPhotoR2
            // 
            this.PBPhotoR2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhotoR2.Location = new System.Drawing.Point(727, 59);
            this.PBPhotoR2.Name = "PBPhotoR2";
            this.PBPhotoR2.Size = new System.Drawing.Size(110, 68);
            this.PBPhotoR2.TabIndex = 23;
            this.PBPhotoR2.TabStop = false;
            // 
            // BParcourir3
            // 
            this.BParcourir3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BParcourir3.Location = new System.Drawing.Point(843, 133);
            this.BParcourir3.Name = "BParcourir3";
            this.BParcourir3.Size = new System.Drawing.Size(78, 68);
            this.BParcourir3.TabIndex = 24;
            this.BParcourir3.Text = "Parcourir";
            this.BParcourir3.UseVisualStyleBackColor = true;
            this.BParcourir3.Click += new System.EventHandler(this.BParcourirR3_click);
            // 
            // NomFichR3
            // 
            this.NomFichR3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomFichR3.Location = new System.Drawing.Point(604, 133);
            this.NomFichR3.Multiline = true;
            this.NomFichR3.Name = "NomFichR3";
            this.NomFichR3.ReadOnly = true;
            this.NomFichR3.Size = new System.Drawing.Size(117, 68);
            this.NomFichR3.TabIndex = 25;
            // 
            // PBPhotoR3
            // 
            this.PBPhotoR3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhotoR3.Location = new System.Drawing.Point(727, 133);
            this.PBPhotoR3.Name = "PBPhotoR3";
            this.PBPhotoR3.Size = new System.Drawing.Size(110, 68);
            this.PBPhotoR3.TabIndex = 26;
            this.PBPhotoR3.TabStop = false;
            // 
            // LEtat
            // 
            this.LEtat.AutoSize = true;
            this.LEtat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LEtat.Location = new System.Drawing.Point(604, 204);
            this.LEtat.Name = "LEtat";
            this.LEtat.Size = new System.Drawing.Size(117, 74);
            this.LEtat.TabIndex = 27;
            this.LEtat.Text = "Etat ";
            this.LEtat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Etat
            // 
            this.Etat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Etat.FormattingEnabled = true;
            this.Etat.Items.AddRange(new object[] {
            "Mauvais",
            "Assez Bon",
            "Bon",
            "Très Bon",
            "Excéllent"});
            this.Etat.Location = new System.Drawing.Point(727, 207);
            this.Etat.Name = "Etat";
            this.Etat.Size = new System.Drawing.Size(110, 21);
            this.Etat.TabIndex = 28;
            // 
            // AddJeux
            // 
            this.ClientSize = new System.Drawing.Size(932, 450);
            this.Controls.Add(this.tabContrel1);
            this.Name = "AddJeux";
            this.tabContrel1.ResumeLayout(false);
            this.SaisieJeu.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBoxRecond.ResumeLayout(false);
            this.groupBoxRecond.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhoto)).EndInit();
            this.SaisieJeuRetro.ResumeLayout(false);
            this.SaisieJeuRetro.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR)).EndInit();
            this.groupBoxNotice.ResumeLayout(false);
            this.groupBoxNotice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhotoR3)).EndInit();
            this.ResumeLayout(false);

        }

        public void initialiseGenre()
        {
            /*this.Liste*/
        }

        public void BParcourir_click(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fichier = dlg.FileName;
                //Affichage nom du fichier en lecture seul
                NomFich.Text = fichier;
                //On crée l'image
                this.photo = Image.FromFile(fichier);
                //on affiche l'image dans le PictureBox et on redimentionne
                this.PBPhoto.Image = photo.GetThumbnailImage(PBPhoto.Width, PBPhoto.Height, null, IntPtr.Zero);
            }
        }

        public void BParcourirR_click(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fichier = dlg.FileName;
                //Affichage nom du fichier en lecture seul
                NomFichR.Text = fichier;
                //On crée l'image
                this.photo1 = Image.FromFile(fichier);
                //on affiche l'image dans le PictureBox et on redimentionne
                this.PBPhotoR.Image = photo1.GetThumbnailImage(PBPhotoR.Width, PBPhotoR.Height, null, IntPtr.Zero);
            }
        }

        public void BParcourirR1_click(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fichier = dlg.FileName;
                //Affichage nom du fichier en lecture seul
                NomFichR1.Text = fichier;
                //On crée l'image
                this.photo2 = Image.FromFile(fichier);
                //on affiche l'image dans le PictureBox et on redimentionne
                this.PBPhotoR1.Image = photo2.GetThumbnailImage(PBPhotoR1.Width, PBPhotoR1.Height, null, IntPtr.Zero);
                this.lph.Add(this.photo2);
            }
        }

        public void BParcourirR2_click(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fichier = dlg.FileName;
                //Affichage nom du fichier en lecture seul
                NomFichR2.Text = fichier;
                //On crée l'image
                this.photo3 = Image.FromFile(fichier);
                //on affiche l'image dans le PictureBox et on redimentionne
                this.PBPhotoR2.Image = photo3.GetThumbnailImage(PBPhotoR2.Width, PBPhotoR2.Height, null, IntPtr.Zero);
                this.lph.Add(this.photo3);
            }
        }

        public void BParcourirR3_click(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fichier = dlg.FileName;
                //Affichage nom du fichier en lecture seul
                NomFichR3.Text = fichier;
                //On crée l'image
                this.photo4 = Image.FromFile(fichier);
                //on affiche l'image dans le PictureBox et on redimentionne
                this.PBPhotoR3.Image = photo1.GetThumbnailImage(PBPhotoR3.Width, PBPhotoR3.Height, null, IntPtr.Zero);
                this.lph.Add(this.photo4);
            }
        }

        public void BAnnuler_click(Object sender, EventArgs e)
        {
            this.Close();
        }


        private void BValider_Click(object sender, EventArgs e)
        {
            float prix;
            bool recond = false;
            DateTime ds = dateTimePicker1.Value;
            Categorie cat = Categorie.Autres;
            try
            {
                cat = (Categorie)Enum.Parse(typeof(Categorie), ListeGenres.SelectedItem.ToString());
            } catch(Exception ex)
            {
                 cat = Categorie.Autres;
            }
            if (Prix.Text != "")
                prix = Single.Parse(Prix.Text);
            else prix = 0;
            if (RBNRecond.Checked) recond = true;
            this.leJeu = new Jeu(Nom.Text, Description.Text, Editeur.Text, Plateforme.Text, cat, prix, ds, recond, this.photo);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BValiderR_Click(object sender, EventArgs e)
        {
            float prix;
            bool recond = true; bool notice=false;
            DateTime ds = dateTimePicker2.Value;
            Categorie cat = Categorie.Autres;
            string etat;
            try
            {
                cat = (Categorie)ListeGenresR.SelectedItem;
            }
            catch (Exception ex)
            {
                cat = Categorie.Autres;
            }

            try
            {
                 etat = Etat.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                etat = "Bon";
            }

            if (PrixR.Text != "")
                prix = Single.Parse(PrixR.Text);
            else prix = 0;
            if (RBNNotice.Checked) notice = true;
            this.leJeuR = new JeuRetro(NomR.Text, DescriptionR.Text, EditeurR.Text, PlateformeR.Text, cat, prix, ds, recond, this.photo1,etat,notice,this.lph);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

    }
}
