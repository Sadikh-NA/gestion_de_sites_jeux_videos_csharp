﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPCDAAA
{
    public partial class Form1 : Form
    {
        private Catalogue leCat; // pour gérer les jeux du catalogue
        private ImageList lph; // pour gérer les images de la liste comportant les photos des jeux
        //private Visualisation visualisation;

        public Catalogue LeCat { get => leCat; set => leCat = value; }
        public ImageList Lph { get => lph; set => lph = value; }

        public Form1()
        {
            //date actuel
            //Image photo= Image.FromFile("Images\\java.jpg");
            DateTime dateJ = DateTime.Now;
            Image duty = (Image)(Properties.Resources.duty);
            Image naruto = (Image)(Properties.Resources.naruto);
            Image gta = (Image)(Properties.Resources.gta);
            Image mario = (Image)(Properties.Resources.mario);
            Image dbz = (Image)(Properties.Resources.dgbz);
            JeuRetro jeux = new JeuRetro("Tarzan", "Tanzan Bros. est un jeu d'arcade développé et édité par Nintendo en 1983", "Shigeru Miyamoto", "Plateforme", Categorie.Course, 9, dateJ, true, duty, "Bon", true,null);
            JeuRetro jeux1 = new JeuRetro("Naruto ", "Naruto est de combat ", "Azerty", "Ordinateur", Categorie.Action, 17, dateJ, true, naruto, "Tres Bon", true, null);

            //Instnciation de quelques objets Jeu
            Jeu jeu1 = new Jeu("Mario", "Mario Bros. est un jeu d'arcade développé et édité par Nintendo en 1983", "Shigeru Miyamoto", "Plateforme", Categorie.Course, 9, dateJ, true, mario);
            Jeu jeu2 = new Jeu("Dragon Ball Z", "Dragon Ball Z est de combat ", "Azerty", "Ordinateur", Categorie.Action, 17, dateJ, true, dbz);
            Jeu jeu3 = new Jeu("GTA", "Grand Therf Auto. est un jeu de combat, de mission", "Moimeme", "Plateforme", Categorie.Autres, 54, dateJ, true, gta);
            InitializeComponent();
            leCat = new Catalogue();
            leCat.AjoutJeu(jeu1);
            leCat.AjoutJeu(jeu2);
            leCat.AjoutJeu(jeu3);
            leCat.AjoutJeu(jeux);
            leCat.AjoutJeu(jeux1);
            InitListeGenres();
            InitListeJeux();
            InitListePhotos();
        }

        private void Ajouter_Click(object sender, EventArgs e)
        {
            AddJeux diag = new AddJeux(); // pour gérer l'ajout de jeux
            DialogResult res = diag.ShowDialog();
            if (res == DialogResult.OK)
            {
                if (diag.LeJeu.Nom != "")
                {
                    LeCat.AjoutJeu(diag.LeJeu);
                    InitListeJeux();
                    this.Edition.Text = diag.LeJeu.ToString();
                }
                else
                {
                    LeCat.AjoutJeu(diag.LeJeuR);
                    InitListeJeux();
                    this.Edition.Text = diag.LeJeu.ToString();
                }
                
            }
        }

        private void Modifier_Click(object sender, EventArgs e)
        {
            // on passe en paramètre LesJeux de type LesJeux du catalogue.
            Modification MajDlg = new Modification(this.leCat.LesJeux);
            MajDlg.ShowDialog(); // appel de la boite en mode modal (préemptif)
                               // Show mode non modal =>le programme continue même si la boite est ouverte
        }

        private void InitListeJeux()
        {
            //remplir la listBox ListJeux, avec les noms des jeux
            foreach (Jeu j in leCat.LesJeux.Liste)
            {
                this.ListeJeux.Items.Add(j.Nom);
            }
        }

        private void InitListeGenres()
        {
            //remplir la comboBox listeGenres avec les noms de genres avec le type énuméré
            //Message initiale Text de la ComboBox
            this.ListeGenres.Text = "Sélectionnez Genre";
            //Liste des selections
            this.ListeGenres.DataSource = Enum.GetValues(typeof(Categorie));
        }


        // Methode qui affiche une fois appuyer sur le Bouton Tous les Jeux 
        private void BTous_click(object sender, EventArgs e)
        {
            //Afficher les informations de tous les jeux dans la zone Edition
            this.Edition.Text = leCat.LesJeux.ToString();
        }


        //// Methode qui affiche une fois appuyer sur le Bouton JeuxRetro 
        private void BRetro_click(object sender, EventArgs e)
        {
            this.Edition.Text = leCat.LesJeux.ToStrings();
        }
        
        // Methode qui affiche une fois appuyer sur Jeux de Visualiser
        private void Visualiser_Click(object sender, EventArgs e)
        {
            Visualisation diag = new Visualisation(this.leCat.LesJeux);
            diag.ShowDialog();
        }

        private void ListeJeux_SelectedIndexChanged(object sender, EventArgs e)
        {
            Jeu jeu = leCat.LesJeux.GetJeuNom(ListeJeux.SelectedItem.ToString());
            this.Edition.Text = jeu.ToString();
            this.Photo.Image = jeu.Photo.GetThumbnailImage(jeu.Photo.Width, jeu.Photo.Height, null, IntPtr.Zero);
        }

        private void ListeGenres_SelectedIndexChanged(object sender, EventArgs e)
        {
            Categorie cat = (Categorie)Enum.Parse(typeof(Categorie), ListeGenres.SelectedItem.ToString());
            
            LesJeux jeux = leCat.LesJeux.GetJeuxGenre(cat);
            this.Edition.Text = jeux.ToString();
        }

        private void supprimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // on passe en paramètre LesJeux de type LesJeux du catalogue.
            Suppression SuppDlg = new Suppression(this.leCat.LesJeux);
            DialogResult res = SuppDlg.ShowDialog();
            if(res == DialogResult.OK)
            {
                leCat.LesJeux.SupprimeJeu(SuppDlg.Jeu);
                this.Edition.Text = leCat.LesJeux.ToString();
            }
        }

        private void InitListePhotos()
        {
            //this.ListePhotos.DrawMode = DrawMode.OwnerDrawVariable;
            this.lph = new ImageList();
            lph.ImageSize = new Size(200, 100);
            foreach (Jeu jeu in this.leCat.LesJeux.Liste)
            {

                this.lph.Images.Add(jeu.Photo);
                this.ListePhotos.Items.Add(jeu.Photo);
                this.ListePhotos.ItemHeight = 100;
            }
        }

        private void ListePhotos_SelectedIndexChanged(object sender, EventArgs e)
        {
            string photo = this.ListePhotos.SelectedItem.ToString();
            foreach(Jeu jeu in this.LeCat.LesJeux.Liste)
            {
                if (jeu.Nom.Equals(photo))
                {
                    this.Edition.Text = jeu.ToString();
                    this.Photo.Image = jeu.Photo.GetThumbnailImage(this.Photo.Width, this.Photo.Height, null, IntPtr.Zero);
                }
            }
        }

        private void BAccessoire_Click(object sender, EventArgs e)
        {
            this.Edition.Text = leCat.LesJeux.AfficherAccessoire();
        }

        private void Accessoire_Click(object sender, EventArgs e)
        {
            AddAccessoire diag = new AddAccessoire(this.leCat.LesJeux); // pour gérer l'ajout de jeux
            DialogResult res = diag.ShowDialog();
            if (res == DialogResult.OK)
            {
                if (diag.LeAcc.Nom != "")
                {
                    LeCat.AjoutAccessoire(diag.LeAcc);
                    leCat.LesJeux.Trier();
                    this.Edition.Text = diag.LeAcc.ToString();
                }
            }
        }

    }
}
