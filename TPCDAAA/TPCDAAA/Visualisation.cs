﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace TPCDAAA
{
    public class Visualisation : Form
    {
        private Jeu leJeu;
        private Catalogue leCat;
        private ImageList lph;
        private Image photo1;
        private JeuRetro JR;
        private SplitContainer splitContainer1;
        private TreeView Arbre;
        private SplitContainer splitContainer2;
        private RichTextBox Edition;
        private PictureBox PBPhoto;
        private LesJeux lesJeux;
        public Visualisation(LesJeux lesJeux)
        {
            InitializeComponent();
            this.lesJeux = lesJeux;
            initArbre();

        }

        public Jeu LeJeu { get => leJeu; set => leJeu = value; }
        public Catalogue LeCat { get => leCat; set => leCat = value; }
        public ImageList Lph { get => lph; set => lph = value; }
        public Image Photo1 { get => photo1; set => photo1 = value; }
        public JeuRetro JR1 { get => JR; set => JR = value; }
        //public LesJeux LesJeux { get => lesJeux; set => lesJeux = value; }

        private void InitializeComponent()
        { 
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Arbre = new System.Windows.Forms.TreeView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Edition = new System.Windows.Forms.RichTextBox();
            this.PBPhoto = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Arbre);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(803, 308);
            this.splitContainer1.SplitterDistance = 267;
            this.splitContainer1.TabIndex = 0;
            // 
            // Arbre
            // 
            this.Arbre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Arbre.Location = new System.Drawing.Point(0, 0);
            this.Arbre.Name = "Arbre";
            this.Arbre.Size = new System.Drawing.Size(267, 308);
            this.Arbre.TabIndex = 0;
            this.Arbre.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Arbre_AfterSelect);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Edition);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.PBPhoto);
            this.splitContainer2.Size = new System.Drawing.Size(532, 308);
            this.splitContainer2.SplitterDistance = 348;
            this.splitContainer2.TabIndex = 0;
            // 
            // Edition
            // 
            this.Edition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Edition.Location = new System.Drawing.Point(0, 0);
            this.Edition.Name = "Edition";
            this.Edition.Size = new System.Drawing.Size(348, 308);
            this.Edition.TabIndex = 0;
            this.Edition.Text = "";
            // 
            // PBPhoto
            // 
            this.PBPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhoto.Location = new System.Drawing.Point(0, 0);
            this.PBPhoto.Name = "PBPhoto";
            this.PBPhoto.Size = new System.Drawing.Size(180, 308);
            this.PBPhoto.TabIndex = 0;
            this.PBPhoto.TabStop = false;
            // 
            // Visualisation
            // 
            this.ClientSize = new System.Drawing.Size(803, 308);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Visualisation";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PBPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        private void initArbre()
        {
            foreach (Categorie cr in Enum.GetValues(typeof(Categorie)))
            {
                TreeNode nd = new TreeNode(cr.ToString());
                LesJeux l = this.lesJeux.GetJeuxGenre(cr);
                foreach (Jeu j in l.Liste)
                {
                    TreeNode na = new TreeNode(j.Nom);
                    nd.Nodes.Add(na);
                }
                Arbre.Nodes.Add(nd);
            }
        }

        private void Arbre_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode nd = e.Node;
            if (nd.Level > 0)
            {
                Jeu j = this.lesJeux.GetJeuNom(nd.Text);
                if (j != null)
                {
                    Edition.Text = j.ToString();
                    if(j.Photo!= null)
                    {
                        PBPhoto.Image = j.Photo.GetThumbnailImage(PBPhoto.Width, PBPhoto.Height, null, IntPtr.Zero);
                    }else
                    {
                        PBPhoto.Image = null;
                    }
                    
                }
            }
        }
    }
}
