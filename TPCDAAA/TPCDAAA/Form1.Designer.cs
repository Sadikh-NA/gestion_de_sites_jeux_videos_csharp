﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace TPCDAAA
{
    public partial class Form1: Form
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Edition = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.BTous = new System.Windows.Forms.Button();
            this.BRetro = new System.Windows.Forms.Button();
            this.LGenre = new System.Windows.Forms.Label();
            this.ListeGenres = new System.Windows.Forms.ComboBox();
            this.ListeJeux = new System.Windows.Forms.ListBox();
            this.Photo = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gérerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouter = new System.Windows.Forms.ToolStripMenuItem();
            this.Modifier = new System.Windows.Forms.ToolStripMenuItem();
            this.visualiserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Jeux = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListePhotos = new System.Windows.Forms.ListBox();
            this.BAccessoire = new System.Windows.Forms.Button();
            this.jeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accessoireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Photo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(800, 426);
            this.splitContainer1.SplitterDistance = 199;
            this.splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.ListePhotos, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.16279F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.83721F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(199, 426);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Edition);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer2.Size = new System.Drawing.Size(597, 426);
            this.splitContainer2.SplitterDistance = 403;
            this.splitContainer2.TabIndex = 0;
            // 
            // Edition
            // 
            this.Edition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Edition.Location = new System.Drawing.Point(0, 0);
            this.Edition.Name = "Edition";
            this.Edition.Size = new System.Drawing.Size(403, 426);
            this.Edition.TabIndex = 0;
            this.Edition.Text = "\n";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.BTous, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.BRetro, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.LGenre, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.ListeGenres, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.ListeJeux, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.Photo, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.BAccessoire, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.53801F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.46199F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(190, 426);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // BTous
            // 
            this.BTous.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTous.Location = new System.Drawing.Point(3, 3);
            this.BTous.Name = "BTous";
            this.BTous.Size = new System.Drawing.Size(184, 53);
            this.BTous.TabIndex = 0;
            this.BTous.Text = "Tous les Jeux";
            this.BTous.UseVisualStyleBackColor = true;
            this.BTous.Click += new System.EventHandler(this.BTous_click);
            // 
            // BRetro
            // 
            this.BRetro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BRetro.Location = new System.Drawing.Point(3, 62);
            this.BRetro.Name = "BRetro";
            this.BRetro.Size = new System.Drawing.Size(184, 56);
            this.BRetro.TabIndex = 1;
            this.BRetro.Text = "Jeux Rétro";
            this.BRetro.UseVisualStyleBackColor = true;
            this.BRetro.Click += new System.EventHandler(this.BRetro_click);
            // 
            // LGenre
            // 
            this.LGenre.AutoSize = true;
            this.LGenre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LGenre.Location = new System.Drawing.Point(3, 180);
            this.LGenre.Name = "LGenre";
            this.LGenre.Size = new System.Drawing.Size(184, 46);
            this.LGenre.TabIndex = 2;
            this.LGenre.Text = "Sélectionner un genre";
            this.LGenre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ListeGenres
            // 
            this.ListeGenres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListeGenres.FormattingEnabled = true;
            this.ListeGenres.Location = new System.Drawing.Point(3, 229);
            this.ListeGenres.Name = "ListeGenres";
            this.ListeGenres.Size = new System.Drawing.Size(184, 21);
            this.ListeGenres.TabIndex = 3;
            this.ListeGenres.SelectedIndexChanged += new System.EventHandler(this.ListeGenres_SelectedIndexChanged);
            // 
            // ListeJeux
            // 
            this.ListeJeux.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListeJeux.FormattingEnabled = true;
            this.ListeJeux.Location = new System.Drawing.Point(3, 256);
            this.ListeJeux.Name = "ListeJeux";
            this.ListeJeux.Size = new System.Drawing.Size(184, 90);
            this.ListeJeux.TabIndex = 4;
            this.ListeJeux.SelectedIndexChanged += new System.EventHandler(this.ListeJeux_SelectedIndexChanged);
            // 
            // Photo
            // 
            this.Photo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Photo.Image = ((System.Drawing.Image)(resources.GetObject("Photo.Image")));
            this.Photo.InitialImage = ((System.Drawing.Image)(resources.GetObject("Photo.InitialImage")));
            this.Photo.Location = new System.Drawing.Point(3, 352);
            this.Photo.Name = "Photo";
            this.Photo.Size = new System.Drawing.Size(184, 71);
            this.Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Photo.TabIndex = 5;
            this.Photo.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gérerToolStripMenuItem,
            this.visualiserToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gérerToolStripMenuItem
            // 
            this.gérerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouter,
            this.Modifier,
            this.supprimerToolStripMenuItem});
            this.gérerToolStripMenuItem.Name = "gérerToolStripMenuItem";
            this.gérerToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.gérerToolStripMenuItem.Text = "Gérer";
            // 
            // ajouter
            // 
            this.ajouter.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jeuToolStripMenuItem,
            this.accessoireToolStripMenuItem});
            this.ajouter.Name = "ajouter";
            this.ajouter.Size = new System.Drawing.Size(180, 22);
            this.ajouter.Text = "Ajouter";
            // 
            // Modifier
            // 
            this.Modifier.Name = "Modifier";
            this.Modifier.Size = new System.Drawing.Size(180, 22);
            this.Modifier.Text = "Modifier";
            this.Modifier.Click += new System.EventHandler(this.Modifier_Click);
            // 
            // visualiserToolStripMenuItem
            // 
            this.visualiserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Jeux});
            this.visualiserToolStripMenuItem.Name = "visualiserToolStripMenuItem";
            this.visualiserToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.visualiserToolStripMenuItem.Text = "Visualiser";
            // 
            // Jeux
            // 
            this.Jeux.Name = "Jeux";
            this.Jeux.Size = new System.Drawing.Size(97, 22);
            this.Jeux.Text = "Jeux";
            this.Jeux.Click += new System.EventHandler(this.Visualiser_Click);
            // 
            // supprimerToolStripMenuItem
            // 
            this.supprimerToolStripMenuItem.Name = "supprimerToolStripMenuItem";
            this.supprimerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.supprimerToolStripMenuItem.Text = "Supprimer";
            this.supprimerToolStripMenuItem.Click += new System.EventHandler(this.supprimerToolStripMenuItem_Click);
            // 
            // ListePhotos
            // 
            this.ListePhotos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListePhotos.Location = new System.Drawing.Point(3, 3);
            this.ListePhotos.Name = "ListePhotos";
            this.ListePhotos.Size = new System.Drawing.Size(193, 420);
            this.ListePhotos.Sorted = true;
            this.ListePhotos.TabIndex = 0;
            // 
            // BAccessoire
            // 
            this.BAccessoire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BAccessoire.Location = new System.Drawing.Point(3, 124);
            this.BAccessoire.Name = "BAccessoire";
            this.BAccessoire.Size = new System.Drawing.Size(184, 53);
            this.BAccessoire.TabIndex = 6;
            this.BAccessoire.Text = "Accessoires";
            this.BAccessoire.UseVisualStyleBackColor = true;
            this.BAccessoire.Click += new System.EventHandler(this.BAccessoire_Click);
            // 
            // jeuToolStripMenuItem
            // 
            this.jeuToolStripMenuItem.Name = "jeuToolStripMenuItem";
            this.jeuToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.jeuToolStripMenuItem.Text = "Jeu";
            this.jeuToolStripMenuItem.Click += new System.EventHandler(this.Ajouter_Click);
            // 
            // accessoireToolStripMenuItem
            // 
            this.accessoireToolStripMenuItem.Name = "accessoireToolStripMenuItem";
            this.accessoireToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.accessoireToolStripMenuItem.Text = "Accessoire";
            this.accessoireToolStripMenuItem.Click += new System.EventHandler(this.Accessoire_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Photo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RichTextBox Edition;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button BTous;
        private System.Windows.Forms.Button BRetro;
        private System.Windows.Forms.Label LGenre;
        private System.Windows.Forms.ComboBox ListeGenres;
        private System.Windows.Forms.ListBox ListeJeux;
        private System.Windows.Forms.PictureBox Photo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gérerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualiserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouter;
        private System.Windows.Forms.ToolStripMenuItem Modifier;
        private ToolStripMenuItem Jeux;
        private ToolStripMenuItem supprimerToolStripMenuItem;
        private ListBox ListePhotos;
        private Button BAccessoire;
        private ToolStripMenuItem jeuToolStripMenuItem;
        private ToolStripMenuItem accessoireToolStripMenuItem;
    }

    
}

