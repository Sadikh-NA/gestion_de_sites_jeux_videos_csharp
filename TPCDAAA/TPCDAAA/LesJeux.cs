﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPCDAAA
{
    //Classe les Jeux
    public class LesJeux
    {
        //attributs
        private List<Jeu> liste;
        private List<Accessoire> listeA;

        //Accesseurs mutateurs
        public List<Jeu> Liste{ get { return this.liste; } set { this.liste = value; } }
        public List<Accessoire> ListeA { get { return this.listeA; } set { this.listeA = value; }}

        //Constructeurs par défauts
        public LesJeux()
        {
            this.liste = new List<Jeu>();
            this.listeA = new List<Accessoire>();
        }


        //Constructeurs avec parametres
        public LesJeux(List<Jeu> liste)
        {
            this.liste = liste;
        }
        public LesJeux(List<Accessoire> listeA)
        {
            this.listeA = listeA;
        }

        //Methode pour ajouter un jeu dans la liste des jeux
        public void AjoutJeu(Jeu j)
        {
            this.liste.Add(j);
        }
        //Methode pour ajouter un jeu dans la liste des jeux
        public void AjoutAccessoire(Accessoire a)
        {
            this.listeA.Add(a);
        }
        //Methode pour supprimer un jeu dans la liste des jeux
        public void SupprimeJeu(Jeu j)
        {
            this.liste.Remove(j);
        }

        //Methode pour supprimer un jeu dans la liste des jeux
        public void SupprimeAccessoire(Accessoire a)
        {
            this.listeA.Remove(a);
        }

        public void Supprime(string j)
        {
            Jeu x=GetJeuNom(j);
            if (x != null) { this.liste.Remove(x); }
            else
            {
               Console.WriteLine("Ce jeu n'existe pas");
            }
        }

        public void SupprimeA(string j)
        {
            Accessoire x = GetAccessoireNom(j);
            Accessoire y = GetAccessoireReference(j);
            if (x != null) { this.listeA.Remove(x); }
            else if(y!=null) { this.listeA.Remove(y); }
            else { Console.WriteLine("Ce jeu n'existe pas"); }
        }

        //Methode pour compter le nombre de jeu dans la liste des jeux
        public int NbJeu()
        {
            return this.liste.Count;
        }

        public LesJeux GetJeuxGenre(Categorie g)
        {
            LesJeux lesJeux = new LesJeux();
            foreach (Jeu unjeu in this.liste)
            {
                if (unjeu.Genre == g)
                {
                    lesJeux.Liste.Add(unjeu);
                }
            }
            return lesJeux;
        }

        //Methode pour recupérer le jeu qui est à l'indice(la position) donner en paramètre
        public Jeu GetJeuIndice(int i)
        {
            return this.liste.ElementAt(i);
        }

        //Methode pour recupérer le jeu qui est à le nom donner en paramètre
        public Jeu GetJeuNom(string nom)
        {
            return (Jeu)this.Liste.Find(j =>(j.Nom.ToLower()).Contains((nom.ToLower())));

        }
        //Methode pour recupérer le jeu qui est à le nom donner en paramètre
        public Accessoire GetAccessoireNom(string nom)
        {
            return (Accessoire)this.ListeA.Find(j => (j.Nom.ToLower()).Contains((nom.ToLower())));

        }

        //Methode pour recupérer le jeu qui est à le nom donner en paramètre
        public Accessoire GetAccessoireReference(string nom)
        {
            return (Accessoire)this.ListeA.Find(j => (j.Nom.ToLower()).Contains((nom.ToLower())));

        }


        //surcharge de la méthode toString
        public override string ToString()
        {
            string s = "";
            foreach (Jeu unjeu in this.liste)
            {
                s += "\n "+unjeu.ToString();
            }
            return s;
        }

        //surcharge de la méthode toString
        public string ToStrings()
        {
            string s = "";
            foreach (Jeu unjeu in this.liste)
            {
                s += "\n " + unjeu.ToString();
            }
            return s;
        }

        //surcharge de la méthode toString
        public string AfficherAccessoire()
        {
            string s = "";
            foreach (Accessoire unjeu in this.listeA)
            {
                s += "\n " + unjeu.ToString();
            }
            return s;
        }

        //La méthode pour Trier les Jeux par prix
        public void Trier()
        {
            //une fonction trie bulle

            //taille de tableau
            int MaxTableau = liste.Count();
            //Parcourir le tableau de l'avant dernier objet au 1er objet JEU
            for (int I = MaxTableau - 2; I >= 0; I--)
            {
                //on part du 1 objet à l'objet actuel 
                for (int J = 0; J <= I; J++)
                {
                    /*
                     * on compare si le prix de l'objet qui est apres lui est inférieur si oui on permute 
                     * sinon on fait rien
                     */
                    //p1.nom.CompareTo(p2.nom)
                    if (liste.ElementAt(J + 1).Nom.CompareTo(liste.ElementAt(J).Nom) < 0)
                    {
                        Jeu temp = liste.ElementAt(J + 1);
                        liste.RemoveAt(J + 1);
                        liste.Insert(J + 1, liste.ElementAt(J));
                        liste.RemoveAt(J);
                        liste.Insert(J, temp);

                    }
                }
            }
        }

    }
}
