﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace TPCDAAA
{
    
    public class Modification : Form
    {
        private LesJeux lesJeux;
        public Modification(LesJeux lesJeux)
        {
            InitializeComponent();
            this.LesJeux = lesJeux;
            initGrille(); // pour construire la grille
        }
        private void InitializeComponent()
        {
            this.Grille = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).BeginInit();
            this.SuspendLayout();
            //
            // Grille
            //
            this.Grille.AllowUserToAddRows = false;
            this.Grille.AllowUserToDeleteRows = false;
            this.Grille.ColumnHeadersHeightSizeMode =
            System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grille.Location = new System.Drawing.Point(0, 0);
            this.Grille.Name = "Grille";
            this.Grille.Size = new System.Drawing.Size(800, 450);
            this.Grille.TabIndex = 0;
            this.Grille.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grille_CellValueChanged);
            //
            // MajDlg
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Grille);
            this.Name = "Modification";
            this.Text = "Modification";
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).EndInit();
            this.ResumeLayout(false);
        }


        private void initGrille()
        {
            // déclaration et allocation des colonnes des grilles
            DataGridViewTextBoxColumn Nom = new DataGridViewTextBoxColumn(); // texte
            Nom.HeaderText = "Nom"; // le titre qui s’affiche de la colonne
            Nom.Name = "Nom"; // nom de la colonne
            Nom.ReadOnly = true;
            DataGridViewTextBoxColumn Description = new DataGridViewTextBoxColumn();
            Description.HeaderText = "Description"; // le titre qui s’affiche de la colonne
            Description.Name = "Description"; // nom de la colonne
            DataGridViewTextBoxColumn Plateforme = new DataGridViewTextBoxColumn();
            Plateforme.HeaderText = "Plateforme"; // le titre qui s’affiche de la colonne
            Plateforme.Name = "Plateforme"; // nom de la colonne
            DataGridViewComboBoxColumn Genre = new DataGridViewComboBoxColumn(); // liste déroulante
            Genre.HeaderText = "Genre"; // le titre qui s’affiche de la colonne
            Genre.Name = "Genre"; // nom de la colonne
            // remplissage de la colonne avec la liste déroulante , on donne les valeurs du type énuméré directement => pratique pour remplir la liste en affectant la « source » de la colonne car c’est une liste
            Genre.DataSource = Enum.GetNames(typeof(Categorie));
            DataGridViewTextBoxColumn Editeur = new DataGridViewTextBoxColumn();
            Editeur.HeaderText = "Editeur"; // le titre qui s’affiche de la colonne
            Editeur.Name = "Editeur"; // nom de la colonne
            Editeur.ReadOnly = true;
            DataGridViewTextBoxColumn Prix = new DataGridViewTextBoxColumn();
            Prix.HeaderText = "Prix"; // le titre qui s’affiche de la colonne
            Prix.Name = "Prix"; // nom de la colonne
            DataGridViewTextBoxColumn DateS = new DataGridViewTextBoxColumn();
            DateS.HeaderText = "DateS"; // le titre qui s’affiche de la colonne
            DateS.Name = "DateS"; // nom de la colonne
            DateS.ReadOnly = true;
            DataGridViewCheckBoxColumn Recond = new DataGridViewCheckBoxColumn(); // case à cocher (booléen)
            Recond.HeaderText = "Disponibilité"; // le titre qui s’affiche de la colonne
            Recond.Name = "Recond"; // nom de la colonne
            DataGridViewImageColumn Image = new DataGridViewImageColumn(); // image
            Image.HeaderText = "Image"; // le titre qui s’affiche de la colonne
            Image.Name = "Image"; // nom de la colonne
            Nom.ReadOnly = true;
            DataGridViewCheckBoxColumn Retro = new DataGridViewCheckBoxColumn();
            Retro.HeaderText = "Retro"; // le titre qui s’affiche de la colonne
            Retro.Name = "Retro"; // nom de la colonne
            DataGridViewTextBoxColumn Etat = new DataGridViewTextBoxColumn();
            Etat.HeaderText = "Etat"; // le titre qui s’affiche de la colonne
            Etat.Name = "Etat"; // nom de la colonne
            DataGridViewCheckBoxColumn Notice = new DataGridViewCheckBoxColumn();
            Notice.HeaderText = "Notice"; // le titre qui s’affiche de la colonne
            Notice.Name = "Notice"; // nom de la colonne
            /*// on paramètre les colonnes
            Genre.HeaderText = "Genre";
            Genre.Name = "Genre";*/
            

            Grille.Columns.AddRange(new DataGridViewColumn[] { Nom, Description, Plateforme, Genre, Editeur, Prix, DateS, Recond, Image, Retro, Etat, Notice });
            Grille.Rows.Add(this.lesJeux.Liste.Count);
            //Grille.Rows.CellDoubleClick += new System.EventHandler(this.Grille_CellValueChanged);

            int i = 0; //compteur de lignes
            foreach (Jeu j in this.lesJeux.Liste)
            {
                // remplissage d’une ligne
                /*Rows => collection des lignes i => i eme
                Cells => collections des cellules de la ligne
                Cells["Nom"] => cellule pour cette colonne => on peut utiliser le nom ou l’indice
                Value => contenu de la cellule qui est affecté au nom du jeu */
                Grille.Rows[i].Cells["Nom"].Value = j.Nom;
                Grille.Rows[i].Cells["Description"].Value = j.Description;
                Grille.Rows[i].Cells["Plateforme"].Value = j.Plateforme;
                Grille.Rows[i].Cells["Genre"].Value = j.GenreS;
                Grille.Rows[i].Cells["Editeur"].Value = j.Editeur;
                Grille.Rows[i].Cells["Prix"].Value = j.Prix;
                Grille.Rows[i].Cells["DateS"].Value = j.DateSortie.ToString("dd/MM/yyyy");
                Grille.Rows[i].Cells["Recond"].Value = (bool)j.Disponibilite;
                if(j.Photo != null)
                {
                    Grille.Rows[i].Cells["Image"].Value = j.Photo.GetThumbnailImage(40, 30, null, IntPtr.Zero);
                }else
                {
                    Grille.Rows[i].Cells["Image"].Value = null;
                }

                //Si c’est un jeu retro
                if (j is JeuRetro)
                {
                    Grille.Rows[i].Cells["Retro"].Value = true;
                    Grille.Rows[i].Cells["Etat"].Value = ((JeuRetro)j).Etat;
                    Grille.Rows[i].Cells["Notice"].Value = ((JeuRetro)j).Notice;
                    // utilisation du cast pour accéder aux attributs du jeuRetro
                }
                else
                {
                    Grille.Rows[i].Cells["Retro"].Value = false;
                }
                i++;
            }

        }

        private void Grille_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int lig = e.RowIndex;
            int col = e.ColumnIndex;
            /*récupère la cellule de la grille à cette position, en sélectionnant la ligne « lig », dans la collection Rows des
            lignes de la Grille, et la cellule à la position « col », dans la collection Cells de cette ligne.La cellule est de type
            « DataGridViewCell » .*/
            DataGridViewCell cell = Grille.Rows[lig].Cells[col]; // on a un indice pour la colonne
            /*DataGridViewCell est une classe abstraite avec des sous-classes
            DataGridViewTextBoxCell, DataGridViewComboBoxCell, DataGridViewImageCell, etc*/
            Jeu j = this.lesJeux.Liste[lig];
            switch (col)
            { 
                case 0:
                    j.Nom = cell.Value.ToString();
                    break;
                case 1:
                    j.Description = cell.Value.ToString();
                    break;
                case 2:
                    j.Plateforme = cell.Value.ToString();
                    break;
                // colonne 3 _ colonne du genre
                case 3:
                    j.GenreS = cell.Value.ToString();
                    string genre = j.GenreS;
                    /*cell.Value => récupère le genre sélectionné
                    GenreS => propriété le genre du jeu à partir d’un string(Genre, et GenreS)*/
                    //j.Genre = Categorie.genre;
                    break;
                case 4:
                    j.Editeur = cell.Value.ToString();
                    break;
                case 5:
                    j.Prix = Single.Parse(cell.Value.ToString()); // ici, la conversion peut faire une erreur si la personne n’a pas saisi un réel(float)
                    break;
                /*case 6:
                    j.DateSortie = (DateTime)cell.Value.ToString();
                    break;*/
                case 7:
                    j.Disponibilite = (bool)cell.Value;
                    break;
                case 8:
                    j.Photo = (Image)cell.Value;
                    break;
                case 9:
                    if (j is JeuRetro) // test le type du jeu
                        cell.Value = (bool)cell.Value;
                    // on affecte la valeur de la cellule à Retro => on doit caster le contenu de la cellule en bool
                    else 
                    cell.Value = false;
                    Grille.Rows[lig].Cells[10].Value = "";
                    Grille.Rows[lig].Cells[11].Value = false;
                    break;
                case 10:
                    if (j is JeuRetro) // test le type du jeu
                        cell.Value = cell.Value.ToString();
                    // on affecte la valeur de la cellule à Etat
                    else cell.Value = "";
                    break;
                case 11: // existence d’une notice
                    if (j is JeuRetro) // test le type du jeu
                        ((JeuRetro)j).Notice = (bool)cell.Value;
                    // on affecte la valeur de la cellule à Notice => on doit caster le contenu de la cellule en bool
                    else cell.Value = false;
                    break;
            }

        }



        //#endregion
        private System.Windows.Forms.DataGridView Grille;

        public LesJeux LesJeux { get => lesJeux; set => lesJeux = value; }
    }
}
