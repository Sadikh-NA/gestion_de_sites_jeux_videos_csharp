﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TPCDAAA
{ 
    /*
     * Classe Accessoire qui implémente l'interface IEquatable pour de la methode Equals
     */
    public class Accessoire : IEquatable<Accessoire>
    {

        //Déclaration des attributs
        private string nom;
        private string description;
        private string reference;
        private float prix;
        private Jeu associe;
        private DateTime dateSortie;
        private Image photo;

        //Constructeurs par défaut
        public Accessoire()
        {
            this.nom = "";
            this.description = "";
            this.reference = "";
            this.prix = 0;
            this.associe = new Jeu();
        }

        public Accessoire(string nom, string description, string reference, float prix, Jeu associe, DateTime dateSortie, Image photo)
        {
            this.nom = nom;
            this.description = description;
            this.reference = reference;
            this.prix = prix;
            this.associe = associe;
            this.dateSortie = dateSortie;
            this.photo = photo;
        }

        // Accesseurs et mutateurs des attributs
        public string Nom { get => nom; set => nom = value; }
        public string Description { get => description; set => description = value; }
        public string Reference { get => reference; set => reference = value; }
        public DateTime DateSortie { get => dateSortie; set => dateSortie = value; }
        public float Prix { get => prix; set => prix = value; }
        public Image Photo { get => photo; set => photo = value; }
        


        //surcharge de la methode ToString 
        public override string ToString()
        {
            string s = "";
            s += "\n Nom: " + this.nom;
            s += "\n Référence: " + this.reference;
            s += "\n Jeu Associé: " + this.associe.Nom;
            s += "\n Prix: " + this.prix;
            s += "\n Description: " + this.description;
            s += "\n Image: " + this.Photo;
            s += "\n Date de Sortie: " + this.dateSortie;
            return s;
        }



        //Surcharge de la methode Equals de IEquatable
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Accessoire i = obj as Accessoire;
            if (i == null)
                return false;
            else
                return Equals(obj);
        }


        //Implémentation de la methode Equals
        public bool Equals(Accessoire i)
        {
            if (!(i is Accessoire) || i == null)
                return false;
            else
            {
                return this.nom.Equals(i.nom);
            }

        }

        public override int GetHashCode()
        {
            int hashCode = -1904446166;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(nom);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(description);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(reference);
            hashCode = hashCode * -1521134295 + prix.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Jeu>.Default.GetHashCode(associe);
            hashCode = hashCode * -1521134295 + dateSortie.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Image>.Default.GetHashCode(photo);
            return hashCode;
        }
    }
}
