﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TPCDAAA
{
    public partial class AddAccessoire : Form
    {
        private Accessoire leAcc;
        private Jeu leJeu;
        private Catalogue leCat;
        private LesJeux lesJeux;
        private Image photo;
        private TabControl tabContrel1;
        private TabPage SaisieAccessoire;
        private TableLayoutPanel tableLayoutPanel3;
        private Label LNom;
        private TextBox Nom;
        private Label LDescription;
        private TextBox Description;
        private Label LReference;
        private TextBox Reference;
        private Button BAnnuler;
        private TabPage SaisieAccessoireRetro;
        private Label LGenre;
        private ComboBox ListeJeux;
        private Label LPrix;
        private TextBox Prix;
        private Label LDate;
        private DateTimePicker dateTimePicker1;
        private Label Photo;
        private Button BParcourir;
        private TextBox NomFich;
        private Button BValider;
        private TableLayoutPanel tableLayoutPanel1;
        private PictureBox PBPhoto;

        public AddAccessoire(LesJeux lesjeux)
        {
           
            this.leAcc = new Accessoire();
            this.leJeu = new Jeu();
            this.photo = null;
            this.lesJeux = lesjeux;
            InitializeComponent();
            initialiseGenre();
        }

        public AddAccessoire()
        {

            this.leAcc = new Accessoire();
            this.leJeu = new Jeu();
            this.photo = null;
            InitializeComponent();
            initialiseGenre();
        }

        public Jeu LeJeu { get => leJeu; set => leJeu = value; }
        //public Form1 Formule { get => formule; set => formule = value; }

        public Catalogue LeCat { get => leCat; set => leCat = value; }
        public Accessoire LeAcc { get => leAcc; set => leAcc = value; }
        public LesJeux LesJeux { get => lesJeux; set => lesJeux = value; }

        private void InitializeComponent()
        {
            this.tabContrel1 = new System.Windows.Forms.TabControl();
            this.SaisieAccessoire = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.LNom = new System.Windows.Forms.Label();
            this.Nom = new System.Windows.Forms.TextBox();
            this.LDescription = new System.Windows.Forms.Label();
            this.Description = new System.Windows.Forms.TextBox();
            this.LReference = new System.Windows.Forms.Label();
            this.Reference = new System.Windows.Forms.TextBox();
            this.BAnnuler = new System.Windows.Forms.Button();
            this.LGenre = new System.Windows.Forms.Label();
            this.ListeJeux = new System.Windows.Forms.ComboBox();
            this.LPrix = new System.Windows.Forms.Label();
            this.Prix = new System.Windows.Forms.TextBox();
            this.LDate = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Photo = new System.Windows.Forms.Label();
            this.BParcourir = new System.Windows.Forms.Button();
            this.NomFich = new System.Windows.Forms.TextBox();
            this.PBPhoto = new System.Windows.Forms.PictureBox();
            this.BValider = new System.Windows.Forms.Button();
            this.SaisieAccessoireRetro = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabContrel1.SuspendLayout();
            this.SaisieAccessoire.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhoto)).BeginInit();
            this.SaisieAccessoireRetro.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabContrel1
            // 
            this.tabContrel1.Controls.Add(this.SaisieAccessoire);
            this.tabContrel1.Controls.Add(this.SaisieAccessoireRetro);
            this.tabContrel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContrel1.Location = new System.Drawing.Point(0, 0);
            this.tabContrel1.Name = "tabContrel1";
            this.tabContrel1.SelectedIndex = 0;
            this.tabContrel1.Size = new System.Drawing.Size(932, 450);
            this.tabContrel1.TabIndex = 1;
            // 
            // SaisieAccessoire
            // 
            this.SaisieAccessoire.Controls.Add(this.tableLayoutPanel3);
            this.SaisieAccessoire.Location = new System.Drawing.Point(4, 22);
            this.SaisieAccessoire.Name = "SaisieAccessoire";
            this.SaisieAccessoire.Size = new System.Drawing.Size(924, 424);
            this.SaisieAccessoire.TabIndex = 0;
            this.SaisieAccessoire.Text = "SaisieAccessoire";
            this.SaisieAccessoire.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 193F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 201F));
            this.tableLayoutPanel3.Controls.Add(this.LNom, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Nom, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.LDescription, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Description, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.LReference, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.Reference, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.BAnnuler, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.LGenre, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.ListeJeux, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.LPrix, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.Prix, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.LDate, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.dateTimePicker1, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.Photo, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.BParcourir, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.NomFich, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.PBPhoto, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.BValider, 2, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.38554F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.61446F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(924, 424);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // LNom
            // 
            this.LNom.AutoSize = true;
            this.LNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LNom.Location = new System.Drawing.Point(3, 0);
            this.LNom.Name = "LNom";
            this.LNom.Size = new System.Drawing.Size(259, 72);
            this.LNom.TabIndex = 0;
            this.LNom.Text = "Nom?";
            this.LNom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Nom
            // 
            this.Nom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Nom.Location = new System.Drawing.Point(268, 3);
            this.Nom.Multiline = true;
            this.Nom.Name = "Nom";
            this.Nom.Size = new System.Drawing.Size(259, 66);
            this.Nom.TabIndex = 1;
            // 
            // LDescription
            // 
            this.LDescription.AutoSize = true;
            this.LDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LDescription.Location = new System.Drawing.Point(3, 72);
            this.LDescription.Name = "LDescription";
            this.LDescription.Size = new System.Drawing.Size(259, 84);
            this.LDescription.TabIndex = 2;
            this.LDescription.Text = "Description";
            this.LDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Description
            // 
            this.Description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Description.Location = new System.Drawing.Point(268, 75);
            this.Description.Multiline = true;
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(259, 78);
            this.Description.TabIndex = 3;
            // 
            // LReference
            // 
            this.LReference.AutoSize = true;
            this.LReference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LReference.Location = new System.Drawing.Point(3, 156);
            this.LReference.Name = "LReference";
            this.LReference.Size = new System.Drawing.Size(259, 72);
            this.LReference.TabIndex = 4;
            this.LReference.Text = "Reference ?";
            this.LReference.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Reference
            // 
            this.Reference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Reference.Location = new System.Drawing.Point(268, 159);
            this.Reference.Multiline = true;
            this.Reference.Name = "Reference";
            this.Reference.Size = new System.Drawing.Size(259, 66);
            this.Reference.TabIndex = 5;
            // 
            // BAnnuler
            // 
            this.BAnnuler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BAnnuler.Location = new System.Drawing.Point(268, 369);
            this.BAnnuler.Name = "BAnnuler";
            this.BAnnuler.Size = new System.Drawing.Size(259, 52);
            this.BAnnuler.TabIndex = 9;
            this.BAnnuler.Text = "Annuler";
            this.BAnnuler.UseVisualStyleBackColor = true;
            this.BAnnuler.Click += new System.EventHandler(this.BAnnuler_click);
            // 
            // LGenre
            // 
            this.LGenre.AutoSize = true;
            this.LGenre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LGenre.Location = new System.Drawing.Point(533, 0);
            this.LGenre.Name = "LGenre";
            this.LGenre.Size = new System.Drawing.Size(187, 72);
            this.LGenre.TabIndex = 10;
            this.LGenre.Text = "Genre ?";
            this.LGenre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ListeJeux
            // 
            this.ListeJeux.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListeJeux.FormattingEnabled = true;
            this.ListeJeux.Location = new System.Drawing.Point(726, 3);
            this.ListeJeux.Name = "ListeJeux";
            this.ListeJeux.Size = new System.Drawing.Size(195, 21);
            this.ListeJeux.TabIndex = 11;
            // 
            // LPrix
            // 
            this.LPrix.AutoSize = true;
            this.LPrix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LPrix.Location = new System.Drawing.Point(533, 72);
            this.LPrix.Name = "LPrix";
            this.LPrix.Size = new System.Drawing.Size(187, 84);
            this.LPrix.TabIndex = 12;
            this.LPrix.Text = "Prix ?";
            this.LPrix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Prix
            // 
            this.Prix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Prix.Location = new System.Drawing.Point(726, 75);
            this.Prix.Multiline = true;
            this.Prix.Name = "Prix";
            this.Prix.Size = new System.Drawing.Size(195, 78);
            this.Prix.TabIndex = 13;
            // 
            // LDate
            // 
            this.LDate.AutoSize = true;
            this.LDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LDate.Location = new System.Drawing.Point(533, 156);
            this.LDate.Name = "LDate";
            this.LDate.Size = new System.Drawing.Size(187, 72);
            this.LDate.TabIndex = 14;
            this.LDate.Text = "Date de Parution";
            this.LDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker1.Location = new System.Drawing.Point(726, 159);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 20);
            this.dateTimePicker1.TabIndex = 15;
            // 
            // Photo
            // 
            this.Photo.AutoSize = true;
            this.Photo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Photo.Location = new System.Drawing.Point(533, 228);
            this.Photo.Name = "Photo";
            this.Photo.Size = new System.Drawing.Size(187, 67);
            this.Photo.TabIndex = 16;
            this.Photo.Text = "Photo ?";
            this.Photo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BParcourir
            // 
            this.BParcourir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BParcourir.Location = new System.Drawing.Point(726, 231);
            this.BParcourir.Name = "BParcourir";
            this.BParcourir.Size = new System.Drawing.Size(195, 61);
            this.BParcourir.TabIndex = 17;
            this.BParcourir.Text = "Parcourir";
            this.BParcourir.UseVisualStyleBackColor = true;
            this.BParcourir.Click += new System.EventHandler(this.BParcourir_click);
            // 
            // NomFich
            // 
            this.NomFich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NomFich.Location = new System.Drawing.Point(533, 298);
            this.NomFich.Multiline = true;
            this.NomFich.Name = "NomFich";
            this.NomFich.ReadOnly = true;
            this.NomFich.Size = new System.Drawing.Size(187, 65);
            this.NomFich.TabIndex = 18;
            this.NomFich.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PBPhoto
            // 
            this.PBPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PBPhoto.Location = new System.Drawing.Point(726, 298);
            this.PBPhoto.Name = "PBPhoto";
            this.PBPhoto.Size = new System.Drawing.Size(195, 65);
            this.PBPhoto.TabIndex = 19;
            this.PBPhoto.TabStop = false;
            // 
            // BValider
            // 
            this.BValider.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BValider.Location = new System.Drawing.Point(533, 369);
            this.BValider.Name = "BValider";
            this.BValider.Size = new System.Drawing.Size(187, 52);
            this.BValider.TabIndex = 20;
            this.BValider.Text = "Valider";
            this.BValider.UseVisualStyleBackColor = true;
            this.BValider.Click += new System.EventHandler(this.BValider_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.13355F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.86645F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.125F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.875F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(924, 424);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // AddJeux
            // 
            this.ClientSize = new System.Drawing.Size(932, 450);
            this.Controls.Add(this.tabContrel1);
            this.Name = "AddJeux";
            this.tabContrel1.ResumeLayout(false);
            this.SaisieAccessoire.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBPhoto)).EndInit();
            this.SaisieAccessoireRetro.ResumeLayout(false);
            this.SaisieAccessoireRetro.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        public void initialiseGenre()
        {
            List<string> l = new List<string>();
            foreach (Jeu jeu in this.lesJeux.Liste)
            {
                l.Add(jeu.Nom);
            }

            ListeJeux.DataSource = l;
        }

        public void BParcourir_click(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                string fichier = dlg.FileName;
                //Affichage nom du fichier en lecture seul
                NomFich.Text = fichier;
                //On crée l'image
                this.photo = Image.FromFile(fichier);
                //on affiche l'image dans le PictureBox et on redimentionne
                this.PBPhoto.Image = photo.GetThumbnailImage(PBPhoto.Width, PBPhoto.Height, null, IntPtr.Zero);
            }
        }

        public void BAnnuler_click(Object sender, EventArgs e)
        {
            this.Close();
        }


        private void BValider_Click(object sender, EventArgs e)
        {
            float prix;
            Jeu jeu = this.lesJeux.GetJeuNom(ListeJeux.SelectedItem.ToString());
            DateTime ds = dateTimePicker1.Value;
            if (Prix.Text != "")
                prix = Single.Parse(Prix.Text);
            else prix = 0;
            this.leAcc = new Accessoire(Nom.Text, Description.Text, Reference.Text, prix,jeu, ds, this.photo);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
