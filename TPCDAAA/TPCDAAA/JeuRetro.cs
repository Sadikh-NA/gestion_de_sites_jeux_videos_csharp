﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPCDAAA
{
    //Classe JeuRetro qui hérite de la classe jeu
    public class JeuRetro : Jeu
    {

        //attribut nouvelle (car il a tous les attributs que Jeu)
        private string etat;
        private List<Image> lphotos;
        private Boolean notice;

        //constructeur par défaut
        public JeuRetro()
        {
            this.etat = "";
            this.notice = false;
            this.lphotos = null;
        }
        //Constructeur avec parametre
        public JeuRetro(string n, string d, string e, string pl, Categorie c, float p, DateTime dte, Boolean r, Image ph, string etat, Boolean notice, List<Image> lphotos):base(n,d,e,pl,c,p,dte,r,ph)
        {
            this.etat = etat;
            this.notice = notice;
            this.lphotos = new List<Image>();
        }

        //Accesseurs et mutateurs des attributs
        public string Etat { get => etat; set => etat = value; }
        public List<Image> Lphotos { get => lphotos; set => lphotos = value; }
        public bool Notice { get => notice; set => notice = value; }
        public List<Image> ListePhotos { get { return this.lphotos; } }


        //surcharge de la classe ToString
        public override string ToString()
        {
            string s;
            s = "\nJeu retro : ";
            s += base.ToString();
            s += "\nEtat du jeu " + this.etat;
            if (this.notice) s += "\nNotice disponible";
            else s += "\nNotice non disponible";
            s += "\nNombre de photos disponibles " + this.lphotos.Count;
            return s;
        }

        //
        public override void Saisie()
        {
            base.Saisie();
            Console.WriteLine("Etat ?");
            this.etat = Console.ReadLine();
            Console.WriteLine("Notice (o/n ?");
            string rep = Console.ReadLine();
            if (rep == "o" || rep == "O")
                this.notice = true;
            else this.notice = false;
        }

        //methode d'affichage du ToString
        public void afficher()
        {
            Console.WriteLine(this.ToString());
        }
    }
}
