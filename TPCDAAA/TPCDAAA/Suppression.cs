﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace TPCDAAA
{

    public class Suppression : Form
    {
        private LesJeux lesJeux;
        private Jeu jeu;
        private void InitializeComponent()
        {
            this.Grille = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).BeginInit();
            this.SuspendLayout();
            //
            // Grille
            //
            this.Grille.AllowUserToAddRows = false;
            this.Grille.AllowUserToDeleteRows = false;
            this.Grille.ColumnHeadersHeightSizeMode =
            System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grille.Location = new System.Drawing.Point(0, 0);
            this.Grille.Name = "Grille";
            this.Grille.Size = new System.Drawing.Size(800, 450);
            this.Grille.TabIndex = 0;
            //this.Grille.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grille_CellCkeckChanged);
            //
            // MajDlg
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Grille);
            this.Name = "Suppression";
            this.Text = "Suppression";
            ((System.ComponentModel.ISupportInitialize)(this.Grille)).EndInit();
            this.ResumeLayout(false);
        }


        private void initGrille()
        {
            // déclaration et allocation des colonnes des grilles
            DataGridViewTextBoxColumn Nom = new DataGridViewTextBoxColumn(); // texte
            Nom.HeaderText = "Nom"; // le titre qui s’affiche de la colonne
            Nom.Name = "Nom"; // nom de la colonne
            Nom.ReadOnly = true;
            DataGridViewComboBoxColumn Genre = new DataGridViewComboBoxColumn(); // liste déroulante
            Genre.HeaderText = "Genre"; // le titre qui s’affiche de la colonne
            Genre.Name = "Genre"; // nom de la colonne
            // remplissage de la colonne avec la liste déroulante , on donne les valeurs du type énuméré directement => pratique pour remplir la liste en affectant la « source » de la colonne car c’est une liste
            Genre.DataSource = Enum.GetNames(typeof(Categorie));
            DataGridViewTextBoxColumn Prix = new DataGridViewTextBoxColumn();
            Prix.HeaderText = "Prix"; // le titre qui s’affiche de la colonne
            Prix.Name = "Prix"; // nom de la colonne
            DataGridViewTextBoxColumn DateS = new DataGridViewTextBoxColumn();
            DateS.HeaderText = "DateS"; // le titre qui s’affiche de la colonne
            DateS.Name = "DateS"; // nom de la colonne
            DateS.ReadOnly = true;
            DataGridViewCheckBoxColumn Recond = new DataGridViewCheckBoxColumn(); // case à cocher (booléen)
            Recond.HeaderText = "Disponibilité"; // le titre qui s’affiche de la colonne
            Recond.Name = "Recond"; // nom de la colonne
            DataGridViewImageColumn Image = new DataGridViewImageColumn(); // image
            Image.HeaderText = "Image"; // le titre qui s’affiche de la colonne
            Image.Name = "Image"; // nom de la colonne
            Nom.ReadOnly = true;
            DataGridViewCheckBoxColumn Retro = new DataGridViewCheckBoxColumn();
            Retro.HeaderText = "Retro"; // le titre qui s’affiche de la colonne
            Retro.Name = "Retro"; // nom de la colonne
            DataGridViewTextBoxColumn Etat = new DataGridViewTextBoxColumn();
            Etat.HeaderText = "Etat"; // le titre qui s’affiche de la colonne
            Etat.Name = "Etat"; // nom de la colonne
            DataGridViewCheckBoxColumn Notice = new DataGridViewCheckBoxColumn();
            Notice.HeaderText = "Notice"; // le titre qui s’affiche de la colonne
            Notice.Name = "Notice"; // nom de la colonne
            DataGridViewButtonColumn Supprimer = new DataGridViewButtonColumn();
            Supprimer.HeaderText = "Supprimer";
            Supprimer.Name = "Supprimer";
            /*// on paramètre les colonnes
            Genre.HeaderText = "Genre";
            Genre.Name = "Genre";*/


            Grille.Columns.AddRange(new DataGridViewColumn[] { Nom, Prix, DateS, Recond, Image, Retro, Etat, Notice, Supprimer });
            Grille.Rows.Add(this.lesJeux.Liste.Count);
            

            int i = 0; //compteur de lignes
            foreach (Jeu j in this.lesJeux.Liste)
            {
                Grille.Rows[i].Cells["Nom"].Value = j.Nom;
                Grille.Rows[i].Cells["Prix"].Value = j.Prix;
                Grille.Rows[i].Cells["DateS"].Value = j.DateSortie.ToString("dd/MM/yyyy");
                Grille.Rows[i].Cells["Recond"].Value = (bool)j.Disponibilite;
                if (j.Photo != null)
                {
                    Grille.Rows[i].Cells["Image"].Value = j.Photo.GetThumbnailImage(40, 30, null, IntPtr.Zero);
                }
                else
                {
                    Grille.Rows[i].Cells["Image"].Value = null;
                }

                //Si c’est un jeu retro
                if (j is JeuRetro)
                {
                    Grille.Rows[i].Cells["Retro"].Value = true;
                    Grille.Rows[i].Cells["Etat"].Value = ((JeuRetro)j).Etat;
                    Grille.Rows[i].Cells["Notice"].Value = ((JeuRetro)j).Notice;
                }
                else
                {
                    Grille.Rows[i].Cells["Retro"].Value = false;
                }
                Grille.Rows[i].Cells["Supprimer"].Value = "Supprimer";
                i++;
            }

        }

        private void Grille_CellCkeckChanged(object sender, DataGridViewCellEventArgs e)
        {
            int lig = e.RowIndex;
            this.jeu = this.lesJeux.Liste[lig];
            this.DialogResult = DialogResult.OK;
            this.Close();
        }



        //#endregion
        private System.Windows.Forms.DataGridView Grille;

        public Suppression( LesJeux lesjeux)
        {
            this.lesJeux = lesjeux;
            InitializeComponent();
            initGrille();
        }

        public LesJeux LesJeux { get => lesJeux; set => lesJeux = value; }
        public Jeu Jeu { get => jeu; set => jeu = value; }
    }
}
