﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPCDAAA
{

    //Class Catalogue
    public class Catalogue
    {
        //Attributs de la classe
        private DateTime dateMAJ;
        private LesJeux lesJeux;

        //Constructeur par défaut
        public Catalogue()
        {
            this.dateMAJ = new DateTime();
            this.LesJeux = new LesJeux();
        }

        //Constructeur par paramètre
        public Catalogue(LesJeux lesJeux, DateTime dateM)
        {
            this.dateMAJ = dateM;
            this.LesJeux = lesJeux;
        }

        //Accesseurs mutateurs de l'attribut DateMAJ
        public DateTime DateMAJ
        {
            get { return this.dateMAJ; }
            set { this.dateMAJ = value; }
        }

        public LesJeux LesJeux { get => lesJeux; set => lesJeux = value; }

        //methode pour ccompter le nombvre de jeux
        public int GetNbJeu()
        {
            return this.LesJeux.Liste.Count;
        }

        //methode pour ccompter le nombvre de jeux
        public int GetNbAccessoire()
        {
            return this.LesJeux.ListeA.Count;
        }

        //methode d'ajout d'un jeu
        public void AjoutJeu(Jeu j)
        {
            this.LesJeux.AjoutJeu(j);

        }
        public void AjoutAccessoire(Accessoire j)
        {
            this.LesJeux.AjoutAccessoire(j);
            //this.LesJeux.AjoutJeu(j);

        }

        //méthode de suppression d'un jeu
        public void SupprimeJeu(Jeu j)
        {
            this.LesJeux.Liste.Remove(j);
        }

        //méthode de suppression d'un jeu
        public void SupprimeAccessoire(Accessoire j)
        {
            this.LesJeux.ListeA.Remove(j);
        }

        //surcharge de la methode ToString
        public override string ToString()
        {
            string s = "";
            foreach (Jeu unJeu in LesJeux.Liste)
            {
                s += "\n " + unJeu.ToString();
            }
            return s;
        }

        //
        public List<Jeu> GetJeuxGenre(Categorie g)
        {
            List<Jeu> lesjeux = new List<Jeu>();
            foreach (Jeu unJeu in lesjeux)
            {
                if (unJeu.Genre.Equals(g))
                {
                    lesjeux.Add(unJeu);
                }
            }
            return lesjeux;
        }
    }
}
